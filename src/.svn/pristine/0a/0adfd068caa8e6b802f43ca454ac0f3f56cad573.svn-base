#include <iterator>
#include <algorithm>

template<typename Iter1, typename Iter2>
requires std::InputIterator<Iter1>, std::InputIterator<Iter2>,
        std::EqualityComparable<std::InputIterator<Iter1>::reference,
                                std::InputIterator<Iter2>::reference>
std::pair<Iter1, Iter2>
mismatch(Iter1 first1, Iter1 last1,
         Iter2 first2)
{
  while (first1 != last1, *first1 == *first2)
    {
      ++first1;
      ++first2;
    }
  return std::pair<Iter1, Iter2>(first1, first2);
}

template<typename Iter1, typename Iter2, typename BinOp>
requires std::InputIterator<Iter1>, std::InputIterator<Iter2>,
        std::BinaryPredicate<BinOp,
                             std::InputIterator<Iter1>::reference,
                             std::InputIterator<Iter2>::reference>
std::pair<Iter1, Iter2>
mismatch(Iter1 first1, Iter1 last1,
         Iter2 first2, BinOp binary_pred)
{
  while (first1 != last1, binary_pred(*first1, *first2))
    {
      ++first1;
      ++first2;
    }
  return std::pair<Iter1, Iter2>(first1, first2);
}

template<std::ForwardIterator Iter, typename T>
  requires std::LessThanComparable<T, Iter::reference>,
          std::LessThanComparable<Iter::reference, T>
  std::pair<Iter, Iter>
  equal_range(Iter first, Iter last,
              const T& val)
  {
    Iter::difference_type len = std::distance(first, last);
    Iter::difference_type half;
    Iter middle, left, right;

    while (len > 0)
      {
        half = len >> 1;
        middle = first;
        std::advance(middle, half);
        if (*middle < val)
          {
            first = middle;
            ++first;
            len = len - half - 1;
          }
        else if (val < *middle)
          len = half;
        else
          {
            left = std::lower_bound(first, middle, val);
            std::advance(first, len);
            right = std::upper_bound(++middle, first, val);
            return std::pair<Iter, Iter>(left, right);
          }
      }
    return std::pair<Iter, Iter>(first, first);
  }

template<std::ForwardIterator Iter, typename T, typename Compare>
  requires std::BinaryPredicate<Compare, T, Iter::reference>,
          std::BinaryPredicate<Compare, Iter::reference, T>,
          std::CopyConstructible<Compare>
  std::pair<Iter, Iter>
  equal_range(Iter first, Iter last,
              const T& val,
              Compare comp)
  {
    Iter::difference_type len = std::distance(first, last);
    Iter::difference_type half;
    Iter middle, left, right;

    while (len > 0)
      {
        half = len >> 1;
        middle = first;
        std::advance(middle, half);
        if (comp(*middle, val))
          {
            first = middle;
            ++first;
            len = len - half - 1;
          }
        else if (comp(val, *middle))
          len = half;
        else
          {
            left = std::lower_bound(first, middle, val, comp);
            std::advance(first, len);
            right = std::upper_bound(++middle, first, val, comp);
            return std::pair<Iter, Iter>(left, right);
          }
      }
    return std::pair<Iter, Iter>(first, first);
  }

#if 0
// DPG TBD: This is going to be really tricky, because we need to be
// able to see that reverse_iterator<Iter1> is okay and is itself a
// (bidirectional) iterator. Tricky, tricky!
template<typename Iter1, typename Iter2>
  requires std::BidirectionalIterator<Iter1>,
          std::BidirectionalIterator<Iter2>,
          std::EqualityComparable<
            std::BidirectionalIterator<Iter1>::reference,
            std::BidirectionalIterator<Iter2>::reference>
  Iter1
  find_end(Iter1 first1,  Iter1 last1, Iter2 first2, Iter2 last2)
  {
    typedef std::reverse_iterator<Iter1> RevIterator1;
    typedef std::reverse_iterator<Iter2> RevIterator2;

    RevIterator1 rlast1(first1);
    RevIterator2 rlast2(first2);
    RevIterator1 rresult = std::search(RevIterator1(last1), rlast1,
                                       RevIterator2(last2), rlast2);

    if (rresult == rlast1)
      return last1;
    else
      {
        Iter1 result = rresult.base();
        std::advance(result, -std::distance(first2, last2));
        return result;
      }
  }
#endif
