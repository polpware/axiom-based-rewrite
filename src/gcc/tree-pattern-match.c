/* Concept-dependent simplifier for parse phase of GNU compiler.
   Copyright (C) 2006 Free Software Foundation, Inc.
   Hacked by Texas A&M University

   This file is part of GCC.

   GCC is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   GCC is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GCC; see the file COPYING.  If not, write to
   the Free Software Foundation, 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* We are going to move all of the rewriting code into a common
   rewrite module, so that the front-end and the middle-end could
   share it. */
/* The module constructs DFS.  The DFS is defined to be a tuple <S, L,
   k, s0, F>, where S is the set of states, L is the set of symbols
   and k is the set of transition functions, and s0 is the initial
   state, and F is the accepting state. */

/* Challenges:
   1. Rvalue and Lvalue. We have the statement
   op(x, identity_element(op), x). The statement being an Rvalue
   rather than an Lvalue, but the compiler would generate the
   following statement.

   2. Rewriting: (TODO) 
   Given: 
   t1 = f (x, y);
   t2 = g (x, y);
   t3 = f( t1, t2); 



*/

/* The construction procedure is as follow:
   init_dfs ();
   build_dfs (decl);
   pattern_match_dfs (decl);
   redex_constraction (decl); 
*/

#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "tm.h"
#include "tree.h"
#include "tree-inline.h"
#include "langhooks.h"
#include "flags.h"
#include "cgraph.h"
#include "diagnostic.h"
#include "timevar.h"
#include "params.h"
#include "fibheap.h"
#include "intl.h"
#include "tree-pass.h"
#include "hashtab.h"
#include "coverage.h"
#include "ggc.h"
#include "tree-flow.h"
#include "rtl.h"

static unsigned int local_dump_no = 1;

static unsigned int optimize_generic_function_p = 0;

typedef struct axiom_expr *axiom_expr_ptr;
typedef struct pattern_cell *pattern_cell_ptr;

/* axiom entry */
struct axiom_expr 
{
  pattern_cell_ptr lhs; /* start point */
  tree rhs;
  tree fun;
  axiom_expr_ptr next; 
  axiom_expr_ptr chain; 
};

/* pattern cell kind */
enum pattern_cell_kind 
  {
    pck_none  = 0,
    pck_const = 1,
    pck_var   = 2, 
    pck_ref   = 3,
    pck_fun   = 4,
    pck_semifun = 5,
    pck_code  = 6
  };

/* pattern cell */
struct pattern_cell 
{

  enum pattern_cell_kind kind; /* Cell category */
  enum tree_code code;  /* Cell code */
  tree type; /* Type information */
  tree node;
  char *position;  /* Cell position */
  pattern_cell_ptr sibling;
  pattern_cell_ptr next;
  pattern_cell_ptr chain;
};

/* pattern table derived from axioms */
static axiom_expr_ptr middle_end_rules_table = NULL;

static axiom_expr_ptr rules_table_inscope = NULL;

/* a free list of "pattern_cell"s awaiting for re-use */
static pattern_cell_ptr free_pattern_cell = NULL;
 
static axiom_expr_ptr free_axiom_expr = NULL;


/* track the record cell */
static pattern_cell_ptr current_cell = NULL;
static char* current_cell_pos = NULL;

static pattern_cell_ptr local_cell_head = NULL;
static pattern_cell_ptr local_cell = NULL;

/* Current candiate statement to be optimized */
static tree *current_tree_candidate;
static block_stmt_iterator *current_bsi_candidate;
typedef struct map_d *map_d_p;
/* Structure to map a variable to its alias set.  */
/* var and set both are reference types. */
struct map_d
{
  /* Variable and its alias set.  */
  tree source;
  tree dest;
  map_d_p chain;
};

/* A free list of "map_d" */
map_d_p free_map_d_list = NULL;

DEF_VEC_P (pattern_cell_ptr);
DEF_VEC_ALLOC_P (pattern_cell_ptr, heap);

/* parameter stacks for the pattern */
static VEC(pattern_cell_ptr, heap) *pattern_parms_stack = NULL;

/* corresponding arguments for the subject term */ 
static VEC(pattern_cell_ptr, heap) *subject_arguments_stack = NULL;

/* Structure to map a variable to its definition stmt, inlcuding two
   kinds of statements: the copy constructor, assignment and the
   modify statment */
/* var should be a reference variable, the stmt should define its
   content */

/* hash map between address and current_cell */
static htab_t must_alias_mapping;
static htab_t ref_def_mappings; 

typedef char *char_p;

DEF_VEC_P (char_p);
DEF_VEC_ALLOC_P (char_p, heap);

/* Worklist of symbols which might be used in building patterns. */

static VEC(tree, heap) *interesting_symbols;
static VEC(tree, heap) *interesting_symbols_helper;
static VEC(char_p, heap) *interesting_symbols_position;
static VEC(char_p, heap) *interesting_symbols_position_helper;

/* function forward declaration */
static pattern_cell_ptr pattern_cell_make (void);
static axiom_expr_ptr axiom_expr_make (void);
static void axiom_expr_free (axiom_expr_ptr);
static void pattern_cell_free (pattern_cell_ptr);

static unsigned int add_pattern (void) __attribute__((noinline));
static unsigned int add_template (void) __attribute__((noinline));
static void process_interesting_symbols (void) __attribute__((noinline));
static void process_one_symbol (tree) __attribute__((noinline)); 
static char* calc_cell_position (const char*, int);
static void preprocess_function_body (void) __attribute__((noinline)); 
/* MATCH */
static unsigned int match_one_symbol (tree) __attribute__((noinline));
static void match_one_statement_1 (tree *)  __attribute__((noinline)); 
static unsigned int
is_pattern_cell_match (pattern_cell_ptr first, pattern_cell_ptr second) __attribute__((noinline)); 
static unsigned int
is_pattern_cell_match_1 (pattern_cell_ptr first, pattern_cell_ptr second) __attribute__((noinline)); 
static bool solve_equations (void); 
/* DEBUG */
static void print_rewrite_table (void);
static void print_rewrite_table_inscope (void);
static void print_pattern_expr (pattern_cell_ptr cell);
static void print_pattern_cell (pattern_cell_ptr cell);
static void sum_row_and_col (pattern_cell_ptr head, 
                             int *row, 
                             int *col, 
                             int startx, 
                             int starty); 
static void print_equations (void);
/* Create an axiom_cell object.  */

static pattern_cell_ptr pattern_cell_make (void)
{
  pattern_cell_ptr entry;

  if (free_pattern_cell)
    {
      entry = free_pattern_cell;
      free_pattern_cell = entry->chain;
    }
  else
	entry = GGC_NEW (struct pattern_cell);

  entry->kind = pck_none;
  entry->code = 0;
  entry->type = NULL_TREE;
  entry->node = NULL_TREE;
  entry->position = NULL;
  entry->sibling = NULL;
  entry->next = NULL;
  entry->chain = NULL;
  return entry;
}

/* Create an axiom_expr object.  */

static axiom_expr_ptr axiom_expr_make (void)
{
  axiom_expr_ptr entry;

  if (free_axiom_expr)
    {
      entry = free_axiom_expr;
      free_axiom_expr = entry->chain;
    }
  else
	entry = GGC_NEW (struct axiom_expr);

  entry->lhs = NULL;
  entry->rhs = NULL_TREE;
  entry->fun = NULL_TREE;
  entry->next = NULL;
  entry->chain = NULL;

  return entry;
}

/* Put ENTRY back on the free list.  */

static void pattern_cell_free (pattern_cell_ptr entry)
{
  pattern_cell_ptr p;
  if (!entry) return;
  for (p = entry; p->chain; p = p->chain)
    ;
  p->chain = free_pattern_cell;
  free_pattern_cell = entry;
}

/* Put ENTRY back on the free list.  */

static void axiom_expr_free (axiom_expr_ptr entry)
{
  axiom_expr_ptr p;
  if (!entry) return;
  for (p = entry; p; p = p->chain) 
    if (p->lhs)
      pattern_cell_free (p->lhs);

  for (p = entry; p->chain; p = p->chain)
    ;
  p->chain = free_axiom_expr;
  free_axiom_expr = entry;
}

#if 0
/* Initilize the middle-end axiom-based optimizer *//
void init_middle_end_axiom_optimizer (void)
{
  axiom_middle_end_rules = NULL; 
}
#endif

/* Compute the hash value */

static hashval_t hash_map_d (const void *p) 
{
  const map_d_p d = (map_d_p) p;
  return htab_hash_pointer (d->source);
}

/* Comparable function */

static int eq_map_d (const void *p1, const void *p2)
{
  const map_d_p q1 = (map_d_p) p1; 
  const map_d_p q2 = (map_d_p) p2; 
 
  return (q1->source == q2->source);
}

/* Create a map entry */

static map_d_p make_map_d (tree source, tree dest)
{
    map_d_p entry;

    if (free_map_d_list)
    {
	entry = free_map_d_list;
	free_map_d_list = entry->chain;
    }
    else
	entry = GGC_NEW (struct map_d);

    entry->chain = NULL;
    entry->source = source;
    entry->dest = dest;
    return entry;
}

/* Put entry back on the free list */

static void free_map_d (void *node)
{
    const map_d_p entry = (map_d_p) node;
    if (!entry) return;
    entry->chain = free_map_d_list;
    free_map_d_list = entry;
}

/* Process the statement to find out must-alias and definition */
/* Assuming that the code is seqential, no branches. */

static void
preprocess_function_body (void) 
{
  basic_block bb;
  block_stmt_iterator bsi;
  map_d_p *slot; 
  map_d_p entry;
  referenced_var_iterator rvi;
  tree var;

  must_alias_mapping = htab_create (50, hash_map_d, eq_map_d, free_map_d);

  ref_def_mappings =  htab_create (50, hash_map_d, eq_map_d, free_map_d);

  /* Go throught all referenced varaibles */
  FOR_EACH_REFERENCED_VAR (var, rvi) 
    {
      if (TREE_CODE (var) == RESULT_DECL &&
          TREE_CODE (TREE_TYPE (var)) == REFERENCE_TYPE) 
        {
          entry = make_map_d (var, current_function_decl);                
          slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, INSERT);
          *slot = entry;
        }
    }

  /* Go throught all statements */
  FOR_EACH_BB (bb)
    for (bsi = bsi_start (bb); !bsi_end_p (bsi); bsi_next (&bsi))
      {
        tree stmt = bsi_stmt (bsi);
        if (TREE_CODE (stmt) == GIMPLE_MODIFY_STMT ||
            TREE_CODE (stmt) == MODIFY_EXPR)
          {
            tree lhs = GENERIC_TREE_OPERAND (stmt, 0);
            tree rhs = GENERIC_TREE_OPERAND (stmt, 1);

            /* both side refer to the same objects */
            if (is_gimple_variable (lhs) &&
                is_gimple_variable (rhs) &&
                TREE_CODE (TREE_TYPE (lhs)) == REFERENCE_TYPE &&
                TREE_CODE (TREE_TYPE (rhs)) == REFERENCE_TYPE) 
              {
                /* Find one alias */
                entry = make_map_d (rhs, NULL_TREE);                
                slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, NO_INSERT);
                if (slot) 
                  {
                    entry = make_map_d (lhs, (*slot)->dest);
                    slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, INSERT);
                    *slot = entry;
                  }
              }
            /* Left hand side is gimple variable and reference type,
               right side is the address */
            else if (is_gimple_variable (lhs) &&
                     TREE_CODE (TREE_TYPE (lhs)) == REFERENCE_TYPE && 
                     TREE_CODE (rhs) == ADDR_EXPR)
              {
                /* Find one alias */
                entry = make_map_d (lhs, TREE_OPERAND (rhs, 0));                
                slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, INSERT);
                *slot = entry;
              }
            else if (TREE_CODE (lhs) == INDIRECT_REF &&
                     is_gimple_variable (TREE_OPERAND (lhs, 0))) 
              {
                entry = make_map_d (TREE_OPERAND (lhs, 0), NULL_TREE);                
                slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, NO_INSERT);
                /*                gcc_assert (slot); */
                if (slot) 
                  {
                    entry = make_map_d ((*slot)->dest, rhs);                
                    slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, INSERT);
                    *slot = entry;
                  }
              }
            else if (is_gimple_variable (lhs) && lhs != rhs) 
              {
                entry = make_map_d (lhs, stmt);                
                slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, INSERT);
                *slot = entry;
              }
            else if (lhs != rhs)
              {
                entry = make_map_d (lhs, stmt);                
                slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, INSERT);
                *slot = entry;
              }
          }
        else if (TREE_CODE (stmt) == CALL_EXPR) 
          {
            /* special handle for copy constructor */
            tree fndecl = TREE_OPERAND (CALL_EXPR_FN (stmt), 0);

            if ((lang_hooks.check_decl_category (fndecl, 1) ||
                 lang_hooks.check_decl_category (fndecl, 4)) && call_expr_nargs (stmt) >= 2)
              {
                tree arg1 = CALL_EXPR_ARG (stmt, 0);
                tree arg2 = CALL_EXPR_ARG (stmt, 1);
#if 0
                /* addr -- addr */
                if (TREE_CODE (arg1) == ADDR_EXPR && TREE_CODE (arg2) == ADDR_EXPR) 
                  {
                    entry = make_map_d (TREE_OPERAND (arg1, 0), TREE_OPERAND (arg2, 0));
                    slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, INSERT);
                    *slot = entry;                 
                  } 
#endif
                /* addr -- reference */
                if (TREE_CODE (arg1) == ADDR_EXPR) 
                  {
                    entry = make_map_d (TREE_OPERAND (arg1, 0), arg2);
                    slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, INSERT);
                    *slot = entry;                 
                  } 
                /* reference or indirect ref*/
                else if (TREE_CODE (arg1) == INDIRECT_REF && is_gimple_variable (TREE_OPERAND (arg1, 0))) 

                  {
                    entry = make_map_d (TREE_OPERAND (arg1, 0), NULL_TREE);                
                    slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, NO_INSERT);
                    /*                gcc_assert (slot); */
                    if (slot) 
                      {
                        entry = make_map_d ((*slot)->dest, arg2);                
                        slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, INSERT);
                        *slot = entry;
                      }
                  }
                /* reference or indirect ref*/
                else if (TREE_CODE (TREE_TYPE (arg1)) == REFERENCE_TYPE) 
                  {
                    entry = make_map_d (arg1, NULL_TREE);                
                    slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, NO_INSERT);
                    /*                gcc_assert (slot); */
                    if (slot) 
                      {
                        entry = make_map_d ((*slot)->dest, arg2);                
                        slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, INSERT);
                        *slot = entry;
                      }
                  }
              }
          }
      } 
}

/* A psss for the axiom optimizer */


/* The pattern DFS is contructed in the following way:
   1. figure out the return expression
   2. find out the first function symbol in the reverse order of all
   statements in the axiom_function declartion
   3. foloow the use-def link to define the pattern until we hit the
   const value or zero-arity function symbol or the function
   parameter (which should be a rference) */

/* The pattern matching is done in the following:
   1. See if there is one expression that match one of the first
   item in the DFS
   2. If so, recursively go to the children of the node. To simplify
   the problem, we only consider function symbol and variable
   decls. The function symbol include all kinds of functions in the
   cplusplus, user-defined functions, the built_in operator, the
   cast_expr, etc. The variables include variables, parameters, const,
   and etc. */

/* Macro to safe push both the interesting tree node and its position */
#define PUSH_NODE_AND_POSITION(node,index)                          \
  {                                                                 \
    VEC_safe_push (tree, heap, interesting_symbols, node);          \
    VEC_safe_push (char_p, heap, interesting_symbols_position,      \
                   calc_cell_position (current_cell_pos, index));   \
  }  

#define PUSH_NODE_AND_POSITION_HELPER(node,index)                       \
  {                                                                     \
    VEC_safe_push (tree, heap, interesting_symbols_helper, node);       \
    VEC_safe_push (char_p, heap, interesting_symbols_position_helper,   \
                   calc_cell_position (current_cell_pos, index));       \
  }  

#define POP_AND_PUSH                                                    \
  while (VEC_length (tree, interesting_symbols_helper) > 0)              \
    {                                                                   \
      tree stmt = VEC_pop (tree, interesting_symbols_helper);           \
      current_cell_pos = VEC_pop (char_p, interesting_symbols_position_helper); \
      VEC_safe_push (tree, heap, interesting_symbols, stmt);            \
      VEC_safe_push (char_p, heap, interesting_symbols_position, current_cell_pos); \
    }


/* Todo: In order to avoid repeating computing the same axiom
   function, we might need hash tables */


/* Given the statement list of an axiom function, we construct its
   pattern and build it into the pattern table. */

static unsigned int add_pattern (void) 
{
  basic_block bb;
  block_stmt_iterator bsi;
  axiom_expr_ptr axiom;
  tree stmt = NULL_TREE;
  tree prev = NULL_TREE;

  /* We are only interested in those axiom pattern functions */
  if (!lang_hooks.check_decl_category (current_function_decl, 2)) 
    {
      /* forward to add_template */
      add_template ();
      return 0; 
    }

  if (dump_file) 
    {
      fprintf (dump_file, "\n-------------------------------------------------\n");
      fprintf (dump_file, "\nCreating pattern from function: ");
      print_generic_decl (dump_file, current_function_decl, 1);
    } 

  /* create a new axiom and link it into the list */
  axiom = axiom_expr_make ();
  axiom->chain = middle_end_rules_table;
  middle_end_rules_table = axiom;

  /* Create a NULL pattern cell as head to facilate the following */
  current_cell = pattern_cell_make ();
  axiom->lhs = current_cell;
  current_cell_pos = NULL;
  axiom->fun = current_function_decl;

  /* The strategy only works for the bulit-in type now, whereas, for
     class type, the last one might be other statements,
     e.g. destructor, we need to handle that later. */

  FOR_EACH_BB (bb)
    for (bsi = bsi_start (bb); !bsi_end_p (bsi); bsi_next (&bsi))
      {
        stmt = bsi_stmt (bsi);
        if (TREE_CODE (stmt) == RETURN_EXPR)
          break;
        prev = stmt;
      }

  gcc_assert (TREE_CODE (stmt) == RETURN_EXPR); 

  if (TREE_TYPE ( TREE_TYPE (current_function_decl)) == void_type_node)
    {
      stmt = prev;
      PUSH_NODE_AND_POSITION (stmt, 1);
    }
  else 
    /* push it into the worklist */
    PUSH_NODE_AND_POSITION (GENERIC_TREE_OPERAND (stmt, 0), 1);

  /* Here we have to do some special for those functions with
     potential return slot optimization. */
  /* Return slot optimization */
  /* construct the must alias */
  /* interpret the copy construtible as def-use */

  preprocess_function_body ();

#if 0
  if (TREE_CODE (GENERIC_TREE_OPERAND (stmt, 0)) == RESULT_DECL)
    {
      PUSH_NODE_AND_POSITION (TREE_OPERAND (stmt, 0), 1);
      /* next push the right cell */
    }
  else
#endif  


  /* Manipuate the worklist */
  process_interesting_symbols ();

  if (dump_file) 
    {
      fprintf (dump_file, "\nThe pattern table:");
      print_rewrite_table ();
    } 

  htab_delete (must_alias_mapping);
  htab_delete (ref_def_mappings);

  return 0;
}

/* Given the statement list of an axiom function, we find out the
   statements we are interested in. */
static unsigned int
add_template (void) 
{
  basic_block bb;
  block_stmt_iterator bsi;
  tree stmt = NULL_TREE;
  tree def;
  bool loop_p = true;
  tree prev = NULL_TREE;

  /* We are only interested in those axiom template functions */
  if (!lang_hooks.check_decl_category (current_function_decl, 3))
    return 0; 

  if (dump_file) 
    {
      fprintf (dump_file, "\n-------------------------------------------------\n");
      fprintf (dump_file, "\nCreating template from function: ");
      print_generic_decl (dump_file, current_function_decl, 1);
    } 

  /* The strategy only works for the bulit-in type now, whereas, for
     class type, the last one might be other statements,
     e.g. destructor, we need to handle that later. */

  FOR_EACH_BB (bb)
    for (bsi = bsi_start (bb); !bsi_end_p (bsi); bsi_next (&bsi))
      {
        stmt = bsi_stmt (bsi);
        if (TREE_CODE (stmt) == RETURN_EXPR)
          break;
        prev = stmt;
      }

  gcc_assert (TREE_CODE (stmt) == RETURN_EXPR);

  if (TREE_TYPE ( TREE_TYPE (current_function_decl)) == void_type_node)
    stmt = prev;
  else 
    /* push it into the worklist */
    stmt = GENERIC_TREE_OPERAND (stmt, 0);

  /* Here we have to do some special for those functions with
     potential return slot optimization. */
  /* Return slot optimization */
  /* construct the must alias */
  /* interpret the copy construtible as def-use */

  preprocess_function_body ();

#if 0
  if (TREE_CODE (GENERIC_TREE_OPERAND (stmt, 0)) == RESULT_DECL)
    {
      PUSH_NODE_AND_POSITION (TREE_OPERAND (stmt, 0), 1);
      /* next push the right cell */
    }
#endif  

  /* Assume there is only one statement */
  while (loop_p && stmt &&
         (TREE_CODE (stmt) == GIMPLE_MODIFY_STMT ||
          TREE_CODE (stmt) == MODIFY_EXPR ||
          TREE_CODE (stmt) == RESULT_DECL ||
          TREE_CODE (stmt) == VAR_DECL ||
          TREE_CODE (stmt) == SSA_NAME)) 
    {
      loop_p = false;
      switch (TREE_CODE (stmt)) 
        {
        case VAR_DECL:
        case RESULT_DECL:
          def = get_current_def (stmt);
          if (def) stmt = def;
          else if (TREE_CODE (TREE_TYPE (stmt)) == REFERENCE_TYPE)
            {   /* we look for the definition in the mapping */
              map_d_p entry = make_map_d (stmt, NULL_TREE);
              map_d_p *slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, NO_INSERT);
              if (slot) /* fllow the link */
                {
                  entry = make_map_d ((*slot)->dest, NULL_TREE); 
                  /* next find the definition */
                  slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, NO_INSERT);
                  if (slot) 
                    {
                      loop_p = true;
                      stmt = (*slot)->dest;
                    }
                }
            } 
          else 
            {
              map_d_p entry = make_map_d (stmt, NULL_TREE);
              /* next find the definition */
              map_d_p *slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, NO_INSERT);
              if (slot) 
                {
                  stmt = (*slot)->dest;
                  loop_p = true;
                }
            }
          break;
        case SSA_NAME:
          def = SSA_NAME_DEF_STMT (stmt);
            
          if (def && TREE_CODE (def) == NOP_EXPR &&
              TREE_CODE (TREE_TYPE (stmt)) == REFERENCE_TYPE)
            {
              /* we look for the alias in the mapping */
              map_d_p entry = make_map_d (def, NULL_TREE);
              map_d_p *slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, NO_INSERT);
              if (slot) /* fllow the link */
                {
                  entry = make_map_d ((*slot)->dest, NULL_TREE); 
                  /* next find the definition */
                  slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, NO_INSERT);
                  if (slot) 
                    {
                      stmt = (*slot)->dest;
                      loop_p = true;
                    }
                }
            }
          else if (def && TREE_CODE (def) != NOP_EXPR)
            {
              stmt = def;
              loop_p = true;
            }
          break;
        case GIMPLE_MODIFY_STMT:
        case MODIFY_EXPR:
          stmt = GENERIC_TREE_OPERAND (stmt, 1);
          loop_p = true;
        default:
          break;
        }
    }
  /* stmt should be what we need */
  if (dump_file) 
    {
      fprintf (dump_file, "\nThe template statement:");
      print_generic_stmt (dump_file, stmt, 1);
    } 

  middle_end_rules_table->rhs = stmt;

  htab_delete (must_alias_mapping);
  htab_delete (ref_def_mappings);

  return 0;
}

/* Process all symbols and built the pattern */

static void 
process_interesting_symbols (void) 
{
  while (VEC_length (tree, interesting_symbols) > 0)  
    {
      /* Pull the statement off the worklist */
      tree stmt = VEC_pop (tree, interesting_symbols);
      current_cell_pos = VEC_pop (char_p, interesting_symbols_position);

      /* We might need reclaim resource here */

      process_one_symbol (stmt);
    }
}


/* Macros to create one item in the rule table */

#define PUSH_TREE_CELL(NODE, KIND)              \
  {                                             \
    current_cell->chain = pattern_cell_make ();  \
    current_cell = current_cell->chain;          \
    current_cell->kind = KIND;                  \
    current_cell->code = TREE_CODE (NODE);      \
    current_cell->type = TREE_TYPE (NODE);      \
    current_cell->node = NODE;                  \
    current_cell->position = current_cell_pos;  \
  }

/* Create an appropariate pattern cell for one symbol and push its
   children into the worklist. */

static void
process_one_symbol (tree node) 
{
  enum tree_code code;

  code  = TREE_CODE (node);

  switch (TREE_CODE_CLASS (TREE_CODE (node)))
    {
    case tcc_declaration:
      if (MTAG_P (node))
        {
          /* we should find out its definition */
          PUSH_TREE_CELL (node, pck_code);
        }
      /* Todo: PAMR_DECL could be assigned , we do handle them now */
      else if (TREE_CODE (node) == PARM_DECL) /* Stop anyway */
        PUSH_TREE_CELL (node, pck_var)
      else if (TREE_CODE (node) == VAR_DECL) 
        {
          /* Todo: We need to tell if the variable has definition point or
             not */ 
          tree def = get_current_def (node); 
          if (def) 
            {
              VEC_safe_push (tree, heap, interesting_symbols, def); 
              VEC_safe_push (char_p, heap, interesting_symbols_position, current_cell_pos);  
            }
          else 
            {
              tree stmt = node;
              map_d_p entry = make_map_d (node, NULL_TREE);
              /* next find the definition */
              map_d_p *slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, NO_INSERT);
              if (slot) 
                {
                  stmt = (*slot)->dest;
                  VEC_safe_push (tree, heap, interesting_symbols, stmt);
                  VEC_safe_push (char_p, heap, interesting_symbols_position, current_cell_pos);
                }
              else 
                PUSH_TREE_CELL (node, pck_code);
            }
        }
          else if (TREE_CODE (node) == RESULT_DECL)
        {
          tree def = get_current_def (node);
          if (def) 
            {
              VEC_safe_push (tree, heap, interesting_symbols, def); 
              VEC_safe_push (char_p, heap, interesting_symbols_position, current_cell_pos);  
            }
          else if (TREE_CODE (TREE_TYPE (node)) == REFERENCE_TYPE)
            {   /* we look for the definition in the mapping */
              tree stmt = node;
              map_d_p entry = make_map_d (node, NULL_TREE);
              map_d_p *slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, NO_INSERT);
              if (slot) /* fllow the link */
                {
                  entry = make_map_d ((*slot)->dest, NULL_TREE); 
                  /* next find the definition */
                  slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, NO_INSERT);
                  if (slot) 
                    {
                      stmt = (*slot)->dest;
                      VEC_safe_push (tree, heap, interesting_symbols, stmt);
                      VEC_safe_push (char_p, heap, interesting_symbols_position, current_cell_pos);
                    }
                  else 
                    PUSH_TREE_CELL (node, pck_code);
                }
              else 
                PUSH_TREE_CELL (node, pck_code);
            }
          else /* should consider other case when result_del is record type */
            {
              tree stmt = node;
              map_d_p entry = make_map_d (node, NULL_TREE);
              map_d_p *slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, NO_INSERT);
              if (slot) /* fllow the link */
                {
                  stmt = (*slot)->dest;
                  VEC_safe_push (tree, heap, interesting_symbols, stmt);
                  VEC_safe_push (char_p, heap, interesting_symbols_position, current_cell_pos);
                }
              else 
                PUSH_TREE_CELL (node, pck_code);
            }
        }
      else 
        PUSH_TREE_CELL (node, pck_code);
      
      break;

    case tcc_type:
      /* We might never handle this class */
      PUSH_TREE_CELL (node, pck_code);
      break;

    case tcc_reference:
      if (TREE_CODE (node) == INDIRECT_REF)
        /* A special case for handling reference */
        { 
          PUSH_TREE_CELL (node, pck_ref);
          PUSH_NODE_AND_POSITION (TREE_OPERAND (node, 0), 1);
        }
      else 
        {
          int len = TREE_OPERAND_LENGTH (node);
          int i;

          /* Push a ref expr */
          PUSH_TREE_CELL (node, pck_ref);
          /* The worklist */

          for (i = 0; i < len; i++)
            PUSH_NODE_AND_POSITION_HELPER (TREE_OPERAND (node, i),i + 1);
          POP_AND_PUSH;
        }
      break;          

    case tcc_expression:
    case tcc_comparison:
    case tcc_unary:
    case tcc_binary:
    case tcc_vl_exp:

      if (TREE_CODE (node) == MODIFY_EXPR) 
        {
          VEC_safe_push (tree, heap, interesting_symbols, TREE_OPERAND (node, 1));
          VEC_safe_push (char_p, heap, interesting_symbols_position, current_cell_pos);
        }
      /* We assume no BIND_EXPRs. */

      /* Push an function symbol */


      /* Push its children onto the Worklist */
      else if (TREE_CODE (node) == CALL_EXPR)
        {
          call_expr_arg_iterator iter;
          tree arg;
          int i = 1;

          PUSH_TREE_CELL (node, pck_fun); 
          PUSH_NODE_AND_POSITION_HELPER (TREE_OPERAND (CALL_EXPR_FN (node), 0), 1); 

          /* Question: do we need  CALL_EXPR_STATIC_CHAIN (node) */


          FOR_EACH_CALL_EXPR_ARG (arg, iter, node)
            {
              /* Push into the Worklist */
              PUSH_NODE_AND_POSITION_HELPER (arg, ++i);
            }
          POP_AND_PUSH;
        }
      else if (TREE_CODE (node) == ADDR_EXPR)
        {
          VEC_safe_push (tree, heap, interesting_symbols, TREE_OPERAND (node, 0));
          VEC_safe_push (char_p, heap, interesting_symbols_position, current_cell_pos);
        }
      else
        {
          int len = TREE_OPERAND_LENGTH (node);
          int i;

          PUSH_TREE_CELL (node, pck_fun); 
          for (i = 0; i < len; i++)
            PUSH_NODE_AND_POSITION_HELPER (TREE_OPERAND (node, i), i + 1);
          POP_AND_PUSH;
        }
      break;

    case tcc_statement:
      /* Todo: we dont handle the now */
      break;

    case tcc_gimple_stmt:
      switch (TREE_CODE (GIMPLE_STMT_OPERAND (node, 1))) 
        {
        case PHI_NODE: 
          /* we dont handle them anymore, since they dont have
             the uniform definition point. */
          PUSH_TREE_CELL (GIMPLE_STMT_OPERAND (node, 0), pck_var);
          break;
        default:
          VEC_safe_push (tree, heap, interesting_symbols, GIMPLE_STMT_OPERAND (node, 1));
          VEC_safe_push (char_p, heap, interesting_symbols_position, current_cell_pos);
          break;
        }
      break; 

    case tcc_constant:
      switch (TREE_CODE (node))
        {
        case VECTOR_CST:
          {
            /* Todo: Comparision will be done with pattern_cell_node */

            tree vals = TREE_VECTOR_CST_ELTS (node);
            tree link;
            int i;

            PUSH_TREE_CELL (node, pck_semifun);

            i = 0;
            for (link = vals; link; link = TREE_CHAIN (link), ++i)
              PUSH_NODE_AND_POSITION_HELPER (TREE_VALUE (link), i + 1);
            POP_AND_PUSH;
          }
          break;

        case COMPLEX_CST:
          PUSH_TREE_CELL (node, pck_semifun);

          /* Add its children into the Worklist */
          PUSH_NODE_AND_POSITION (TREE_IMAGPART (node), 2);
          PUSH_NODE_AND_POSITION (TREE_REALPART (node), 1);
          break;

        default: 
          PUSH_TREE_CELL (node, pck_const);
          break;
        }

    case tcc_exceptional:
      switch (TREE_CODE (node))
        {

        case IDENTIFIER_NODE:
          PUSH_TREE_CELL (node, pck_code);
          break;

        case TREE_LIST:
          /* Assume no such no node */
          PUSH_TREE_CELL (node, pck_semifun);
          PUSH_NODE_AND_POSITION (TREE_VALUE (node), 2);
          PUSH_NODE_AND_POSITION (TREE_PURPOSE (node), 1);
          break;

        case TREE_VEC:
          {
            int i; 
            int len;

            PUSH_TREE_CELL (node, pck_semifun);

            len = TREE_VEC_LENGTH (node);
            for (i = 0; i < len; i++)
              if (TREE_VEC_ELT (node, i))
                PUSH_NODE_AND_POSITION_HELPER (TREE_VEC_ELT (node, i), i + 1);
            POP_AND_PUSH;
            break;
          }
        case STATEMENT_LIST:
          /* Refuse to handle */
          break;

        case BLOCK:
          /* Refuse to handle */
          break;

        case SSA_NAME:
          /* Must have definiton point, so we could saftely handle it. */
          /* Here we reuse position */ 
          /* PLease note the subtle difference bt the SSA_NAME and
             VAR_DECL */
          {
            /*            tree stmt = get_current_def (node);
             */
            tree stmt = SSA_NAME_DEF_STMT (node);
            
            if (stmt && TREE_CODE (stmt) == NOP_EXPR && 
                TREE_CODE (TREE_TYPE (node)) == REFERENCE_TYPE)
              {
                /* we look for the alias in the mapping */
                map_d_p entry = make_map_d (node, NULL_TREE);
                map_d_p *slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, NO_INSERT);
                if (slot) /* fllow the link */
                  {
                    entry = make_map_d ((*slot)->dest, NULL_TREE); 
                    /* next find the definition */
                    slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, NO_INSERT);
                    if (slot) 
                      { 
                        stmt = (*slot)->dest;

                        VEC_safe_push (tree, heap, interesting_symbols, stmt);
                        VEC_safe_push (char_p, heap, interesting_symbols_position, current_cell_pos);
                      }
                    else
                      PUSH_TREE_CELL (node, pck_var);
                  }
                else 
                  PUSH_TREE_CELL (node, pck_var);
              }
            else if (stmt && TREE_CODE (stmt) != NOP_EXPR) 
              {
                VEC_safe_push (tree, heap, interesting_symbols, stmt);
                VEC_safe_push (char_p, heap, interesting_symbols_position, current_cell_pos);
              } 
            else 
              PUSH_TREE_CELL (node, pck_var);

            break; 
          }

        case PHI_NODE:
          /* TOdo: should have no such nodes, since we handle it in the
             GIMPLE_MODIFY_TREE. */
          PUSH_TREE_CELL (node, pck_var);
          break;

        default:
          break;
        }
    default:
      break;
    }
}

#undef PUSH_NODE_AND_POSITION
#undef PUSH_NODE_AND_POSITION_HELPER
#undef POP_AND_PUSH
#undef PUSH_TREE_CELL

/* The following is for pattern matching. The basic idea is to go
   through all interesting statements, and try to figure out if it
   match with some pattern or not */

/* Given the statement list of a function, we try to match the
   interesting statement  */

static unsigned int 
do_pattern_match (void)
{
  basic_block bb;
  block_stmt_iterator bsi;
  tree stmt = NULL_TREE;
  tree *subject = NULL;
  axiom_expr_ptr p;

  /* We should exclude those pattern functions */
  if (lang_hooks.check_decl_category (current_function_decl, 2) ||
      lang_hooks.check_decl_category (current_function_decl, 3))
  return 0;

  if (dump_file) 
    {
      fprintf (dump_file, "\n-------------------------------------------------\n");
      fprintf (dump_file, "\nOptmizing function: ");
      print_generic_decl(dump_file, current_function_decl, 1);
    } 

  rules_table_inscope = NULL;
  /* filter out the interesting patterns */
  for (p = middle_end_rules_table; p; p = p->chain)
    {
      if (optimize_generic_function_p) 
        {  
          if (lang_hooks.check_node_info_2 (p->fun, current_function_decl, 2)) 
            {
              p->next = rules_table_inscope;
              rules_table_inscope = p;
            }
        }
      else 
        {
          p->next = rules_table_inscope;
          rules_table_inscope = p;
        }
    }

  if (!rules_table_inscope) return 0;

  if (dump_file) 
    {
      fprintf (dump_file, "\nAll avaliable patterns: ");
      print_rewrite_table_inscope ();
    }

  preprocess_function_body ();

  FOR_EACH_BB (bb)
    for (bsi = bsi_start (bb); !bsi_end_p (bsi); bsi_next (&bsi))
      {
        current_bsi_candidate = &bsi;
        current_tree_candidate = bsi_stmt_ptr (bsi);
        stmt = bsi_stmt (bsi);
        /* print_generic_stmt (stdout, stmt, TDF_DETAILS); */
        if (stmt && TREE_CODE (stmt) != ERROR_MARK &&
            (TREE_CODE (stmt) == MODIFY_EXPR ||
             TREE_CODE (stmt) == GIMPLE_MODIFY_STMT)) 
          {
            tree rhs = GENERIC_TREE_OPERAND (stmt, 1);
            /*
            if (TREE_CODE_CLASS (TREE_CODE (rhs)) != tcc_expression &&
                TREE_CODE_CLASS (TREE_CODE (rhs)) != tcc_comparison &&
                TREE_CODE_CLASS (TREE_CODE (rhs)) != tcc_unary &&
                TREE_CODE_CLASS (TREE_CODE (rhs)) != tcc_binary)
              continue;              
            */
            if (is_gimple_variable (rhs)) continue;
            subject = &GENERIC_TREE_OPERAND (stmt, 1);
            if (TREE_CODE (*subject) != CALL_EXPR) continue; 
            match_one_statement_1 (subject);
          }
        else if (stmt && TREE_CODE (stmt) != ERROR_MARK &&
                 TREE_CODE (stmt) == CALL_EXPR)
          {
            subject = current_tree_candidate;
            match_one_statement_1 (subject);
          }
      }
  return 0;
}

/* member predicate */

static int 
index_of_pattern_parms (tree member)
{
  int ix;
  pattern_cell_ptr ptr;
  for (ix = 0; VEC_iterate (pattern_cell_ptr, pattern_parms_stack, ix, ptr); ix++) 
    {
      if (TREE_CODE (member) == SSA_NAME) 
        {
          /* compare the variable inside */
          if (SSA_NAME_VAR (member) == SSA_NAME_VAR (ptr->node))
            return ix;
        }
      else if (ptr->node == member)
        return ix;
    }
  return -1;
}

/* callback for traversal tree */

static tree
replace_parm_with_arg (tree *tp, int *walk_subtrees, void *data ATTRIBUTE_UNUSED)
{
  enum tree_code code = TREE_CODE (*tp);
  int index;
  pattern_cell_ptr cell;

  if (dump_file)
    { 
      fprintf (dump_file, "\nThe tree node: ");
      print_generic_expr (dump_file, *tp, TDF_DETAILS);
    }
  /* Only consider PARM_DECL */
  if (code == PARM_DECL || code == SSA_NAME) 
    {
      index = index_of_pattern_parms (*tp);
      if (index != -1)
        { 
          cell = VEC_index (pattern_cell_ptr, subject_arguments_stack, index);
          if (dump_file)
            { 
              fprintf (dump_file, "\nFounc cell:");
              print_pattern_cell (cell);
            }
          *tp = cell->node;
          *walk_subtrees = 0;
        }
    }
  return NULL_TREE;
}

/* Go through all patterns and perform rewriting */

static void match_one_statement_1 (tree *stmt) 
{
  axiom_expr_ptr current_axiom, p;
  unsigned int matched;
  tree result_p;

  local_cell_head = pattern_cell_make ();

  for (p = rules_table_inscope; p; p = p->next)
    {

      if (!pattern_parms_stack) 
        pattern_parms_stack = VEC_alloc (pattern_cell_ptr, heap, 10);
      else
        VEC_truncate (pattern_cell_ptr, pattern_parms_stack, 0);
    
      if (!subject_arguments_stack)
        subject_arguments_stack = VEC_alloc (pattern_cell_ptr, heap, 10);
      else
        VEC_truncate (pattern_cell_ptr, subject_arguments_stack, 0);

      current_axiom = p;
      current_cell = p->lhs->chain;

      local_cell = local_cell_head;
      pattern_cell_free (local_cell->chain);
      local_cell->chain = NULL;

      matched = match_one_symbol (*stmt);

      if (matched) 
        {
          matched = solve_equations ();
          if (dump_file) 
            {
              fprintf (dump_file, "\n Pattern:");
              print_pattern_expr (p->lhs);
              fprintf (dump_file, "\nCandidate Matching Statement:");
              print_generic_stmt (dump_file, *stmt, 1);
              fprintf (dump_file, "\n Subject Pattern");
              print_pattern_expr (local_cell_head);
              fprintf (dump_file, "\n Varibles Match Table:");
              print_equations ();
            }
        }
      /* Ready to rewrite */
      if (matched) 
        {
          if (dump_file) 
            {
              fprintf (dump_file, "\nChecking Matching Succeed!");
            }

          result_p = current_axiom->rhs;

          if (dump_file) 
            {
              fprintf (dump_file, "\nThe statement:");
              print_generic_stmt (dump_file, result_p, 1);
            }

          walk_tree (&result_p, &copy_tree_r, NULL, NULL);
          walk_tree (&result_p, &replace_parm_with_arg, NULL, NULL);

          /* Call expr handled specially */
          if (TREE_CODE (*current_tree_candidate) == CALL_EXPR)
            {
              TREE_BLOCK (result_p) = TREE_BLOCK (*current_tree_candidate);
              bsi_replace (current_bsi_candidate, result_p, false);
              break;
            }

          /* Handle the difference between reference and addr */
          if (TREE_CODE (result_p) == INDIRECT_REF &&
              TREE_CODE (TREE_OPERAND (result_p, 0)) == ADDR_EXPR)
            result_p = TREE_OPERAND (TREE_OPERAND (result_p, 0), 0);

          /* It might be that we use reference on the rhs, we change it
             in this case . */
          if ((TREE_CODE (TREE_TYPE (GENERIC_TREE_OPERAND (*current_tree_candidate, 1))) != REFERENCE_TYPE && 
               TREE_CODE (TREE_TYPE (GENERIC_TREE_OPERAND (*current_tree_candidate, 1))) != POINTER_TYPE) &&
              (TREE_CODE (TREE_TYPE (result_p)) == POINTER_TYPE ||
               TREE_CODE (TREE_TYPE (result_p)) == REFERENCE_TYPE))
            {
              result_p = TREE_OPERAND (result_p, 0);
            }
          /* TODO: we might have to consider the lhs is REFERENCE_TYPE,
             whereas the rhs is NON-reference_type. */

          if (dump_file) 
            {
              fprintf (dump_file, "\nChanged into:");
              print_generic_stmt (dump_file, result_p, 1);
            }

          /* Next substitutions */
          *stmt = result_p;

          if (dump_file) 
            {
              fprintf (dump_file, "\nThe whole statement at tage 1:");
              print_generic_stmt (dump_file, *current_tree_candidate, 1);
            }

          /* if the result tree does not satisfy our purpse */
          {
            tree lhs = GENERIC_TREE_OPERAND (*current_tree_candidate, 0);
            tree rhs = GENERIC_TREE_OPERAND (*current_tree_candidate, 1);
            tree expr = lang_hooks.build_special_fn (lhs, rhs, 1);
            if (expr) 
              {
                if (dump_file) 
                  {
                    fprintf (dump_file, "The statement used to bsi_replace is:");
                    print_generic_stmt (dump_file, expr, 1);
                  }
                TREE_BLOCK (expr) = TREE_BLOCK (*current_tree_candidate);
                bsi_replace (current_bsi_candidate, expr, false);
              }
            else bsi_replace (current_bsi_candidate, *current_tree_candidate, false);
          }

          if (dump_file) 
            {
              fprintf (dump_file, "\nThe whole statement at stage 2:");
              print_generic_stmt (dump_file, *current_tree_candidate, 1);
            }
          break;
        }
    }
  pattern_cell_free (local_cell_head);
}

/* Macros to create one item in the rule table */

#define PUSH_TREE_CELL(NODE, KIND)              \
  {                                             \
    local_cell->chain = pattern_cell_make ();  \
    local_cell = local_cell->chain;  \
    local_cell->kind = KIND;                  \
    local_cell->code = TREE_CODE (NODE);      \
    local_cell->type = TREE_TYPE (NODE);      \
    local_cell->node = NODE;                  \
    matched = is_pattern_cell_match (current_cell, local_cell); \
  }

/* Create an appropariate pattern cell for one symbol and push its
   children into the worklist. */

static unsigned int
match_one_symbol (tree node) 
{
  enum tree_code code;

  unsigned int matched;

  code  = TREE_CODE (node);

  switch (TREE_CODE_CLASS (TREE_CODE (node)))
    {
    case tcc_declaration:
      if (MTAG_P (node))
        {
          /* we should find out its definition */
          PUSH_TREE_CELL (node, pck_code);
          return matched;
        }
      /* Todo: PAMR_DECL could be assigned , we do handle them now */
      else if (TREE_CODE (node) == PARM_DECL) /* Stop anyway */
        {
          PUSH_TREE_CELL (node, pck_var);
          return matched;
        }
      else if (TREE_CODE (node) == VAR_DECL) 
        {
          tree def;
          /* if the pattern is a var_node, stop finding definition */
          if (current_cell->kind == pck_var) 
          {
            PUSH_TREE_CELL (node, pck_var);
            return matched;
          }

          /* Todo: We need to tell if the variable has definition point or
             not */ 
          def = get_current_def (node); 
          if (def) 
            { 
              matched =  match_one_symbol (def);
              return matched;
            }
          else 
            {
              tree stmt = node;
              map_d_p entry = make_map_d (node, NULL_TREE);
              /* next find the definition */
              map_d_p *slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, NO_INSERT);
              if (slot) 
                {
                  stmt = (*slot)->dest;
                  matched =  match_one_symbol (stmt);
                  return matched;
                }
              else 
                {
                  PUSH_TREE_CELL (node, pck_code);
                  return matched;
                }
            }
        }
          else if (TREE_CODE (node) == RESULT_DECL)
        {
          tree def;
          /* if the pattern is a var_node, stop finding definition */
          if (current_cell->kind == pck_var) 
          {
            PUSH_TREE_CELL (node, pck_var);
            return matched;
          }

          def = get_current_def (node);
          if (def) 
            {
              matched =  match_one_symbol (def);
              return matched;
            }
          else if (TREE_CODE (TREE_TYPE (node)) == REFERENCE_TYPE)
            {   
              /* we look for the definition in the mapping */
              tree stmt = node;
              map_d_p entry = make_map_d (node, NULL_TREE);
              map_d_p *slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, NO_INSERT);
              if (slot) /* fllow the link */
                {
                  entry = make_map_d ((*slot)->dest, NULL_TREE); 
                  /* next find the definition */
                  slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, NO_INSERT);
                  if (slot) 
                    {
                      stmt = (*slot)->dest;
                      matched =  match_one_symbol (stmt);
                      return matched;
                    }
                  else 
                    {
                      PUSH_TREE_CELL (node, pck_code);
                      return matched;
                    }
                }
              else 
                {
                  PUSH_TREE_CELL (node, pck_code);
                  return matched;
                }
            }
          else /* should consider other case when result_del is record type */
            {
              tree stmt = node;
              map_d_p entry = make_map_d (node, NULL_TREE);
              map_d_p *slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, NO_INSERT);
              if (slot) /* fllow the link */
                {
                  stmt = (*slot)->dest;
                  matched =  match_one_symbol (stmt);
                  return matched;
                }
              else 
                {
                  PUSH_TREE_CELL (node, pck_code);
                  return matched;
                }
            }
        }
      else 
        {
          PUSH_TREE_CELL (node, pck_code);
          return matched;
        }      
      break;

    case tcc_type:
      /* We might never handle this class */
      {
        PUSH_TREE_CELL (node, pck_code);
        return matched;
      }
      break;

    case tcc_reference:
      if (TREE_CODE (node) == INDIRECT_REF)
        /* A special case for handling reference */
        { 
          PUSH_TREE_CELL (node, pck_ref);
          if (matched == 0 || matched == 2 || matched == 3)
            return matched;
          matched = match_one_symbol (TREE_OPERAND (node, 0));
          return matched;
        }
      else 
        {
          int len = TREE_OPERAND_LENGTH (node);
          int i;

          /* Push a ref expr */
          PUSH_TREE_CELL (node, pck_ref);
          /* The worklist */
          if (matched == 0 || matched == 2 || matched == 3)
            return matched;

          for (i = 0; i < len; i++)
            {
              matched = match_one_symbol (TREE_OPERAND (node, i));
              if (matched == 0) return matched;
            }
        }
      break;          

    case tcc_expression:
    case tcc_comparison:
    case tcc_unary:
    case tcc_binary:
    case tcc_vl_exp:

      if (TREE_CODE (node) == MODIFY_EXPR) 
        {
          matched = match_one_symbol (TREE_OPERAND (node, 1));
          if (matched == 0) return matched;
        }
      /* We assume no BIND_EXPRs. */

      /* Push an function symbol */


      /* Push its children onto the Worklist */
      else if (TREE_CODE (node) == CALL_EXPR)
        {
          call_expr_arg_iterator iter;
          tree arg;

          PUSH_TREE_CELL (node, pck_fun);
          if (matched == 0 || matched == 2 || matched == 3)
            return matched;
          matched = match_one_symbol (TREE_OPERAND (CALL_EXPR_FN (node), 0));
          if (matched == 0) return matched;

          /* Question: do we need  CALL_EXPR_STATIC_CHAIN (node) */

          FOR_EACH_CALL_EXPR_ARG (arg, iter, node)
            {
              matched = match_one_symbol (arg);
              if (matched == 0) return matched;
            }
        }

      /* skip the addr_expr */
      else if (TREE_CODE (node) == ADDR_EXPR)
        {
           matched = match_one_symbol (TREE_OPERAND (node, 0));
           return matched;
        }
      else
        {
          int len = TREE_OPERAND_LENGTH (node);
          int i;

          PUSH_TREE_CELL (node, pck_fun); 

          if (matched == 0 || matched == 2 || matched == 3)
            return matched;

          for (i = 0; i < len; i++)
            {
              matched = match_one_symbol (TREE_OPERAND (node, i));
              if (matched == 0) return matched;
            }
        }
      break;

    case tcc_statement:
      /* Todo: we dont handle the now */
      break;

    case tcc_gimple_stmt:
      switch (TREE_CODE (GIMPLE_STMT_OPERAND (node, 1))) 
        {
        case PHI_NODE: 
          /* we dont handle them anymore, since they dont have
             the uniform definition point. */
          PUSH_TREE_CELL (GIMPLE_STMT_OPERAND (node, 0), pck_var);
          if (matched == 0 || matched == 2 || matched == 3)
            return matched;
          break;
        default:
          matched = match_one_symbol (GIMPLE_STMT_OPERAND (node, 1));
          if (matched == 0) return matched;
          break;
        }
      break; 

    case tcc_constant:
      switch (TREE_CODE (node))
        {
        case VECTOR_CST:
          {
            /* Todo: Comparision will be done with pattern_cell_node */

            tree vals = TREE_VECTOR_CST_ELTS (node);
            tree link;
            int i;

            PUSH_TREE_CELL (node, pck_semifun);
            if (matched == 0 || matched == 2 || matched == 3)
              return matched;

            i = 0;
            for (link = vals; link; link = TREE_CHAIN (link), ++i)
              {
                matched = match_one_symbol (TREE_VALUE (link));
                if (matched == 0) return matched;
              }        
          }
          break;

        case COMPLEX_CST:
          PUSH_TREE_CELL (node, pck_semifun);
          if (matched == 0 || matched == 2 || matched == 3)
            return matched;

          matched = match_one_symbol (TREE_REALPART (node));
          if (matched == 0) return matched;
          matched = match_one_symbol (TREE_IMAGPART (node));
          if (matched == 0) return matched;

          break;

        default: 
          PUSH_TREE_CELL (node, pck_const);
          if (matched == 0 || matched == 2 || matched == 3)
            return matched;
          break;
        }

    case tcc_exceptional:
      switch (TREE_CODE (node))
        {

        case IDENTIFIER_NODE:
          PUSH_TREE_CELL (node, pck_code);
          if (matched == 0 || matched == 2 || matched == 3)
            return matched;
          break;

        case TREE_LIST:
          /* Assume no such no node */
          PUSH_TREE_CELL (node, pck_semifun);
          if (matched == 0 || matched == 2 || matched == 3)
            return matched;

          matched = match_one_symbol (TREE_PURPOSE (node));
          if (matched == 0) return matched;
          matched = match_one_symbol (TREE_VALUE (node));
          if (matched == 0) return matched;

          break;

        case TREE_VEC:
          {
            int i; 
            int len;

            PUSH_TREE_CELL (node, pck_semifun);
            if (matched == 0 || matched == 2 || matched == 3)
              return matched;

            len = TREE_VEC_LENGTH (node);
            for (i = 0; i < len; i++)
              if (TREE_VEC_ELT (node, i))
                {
                  matched = match_one_symbol (TREE_VEC_ELT (node, i));
                  if (matched == 0) return matched;
                }
            break;
          }
        case STATEMENT_LIST:
          /* Refuse to handle */
          break;

        case BLOCK:
          /* Refuse to handle */
          break;

        case SSA_NAME:
          /* Must have definiton point, so we could saftely handle it. */
          /* Here we reuse position */ 
          /* PLease note the subtle difference bt the SSA_NAME and
             VAR_DECL */
          {

            tree stmt;
            /* if the pattern is a var_node, stop finding definition */
            if (current_cell->kind == pck_var) 
              {
                PUSH_TREE_CELL (node, pck_var);
                return matched;
              }

            /*            tree stmt = get_current_def (node);
             */
            stmt = SSA_NAME_DEF_STMT (node);
            
            if (stmt && TREE_CODE (stmt) == NOP_EXPR && 
                TREE_CODE (TREE_TYPE (node)) == REFERENCE_TYPE)
              {
                /* we look for the alias in the mapping */
                map_d_p entry = make_map_d (node, NULL_TREE);
                map_d_p *slot = (map_d_p *) htab_find_slot (must_alias_mapping, entry, NO_INSERT);
                if (slot) /* fllow the link */
                  {
                    entry = make_map_d ((*slot)->dest, NULL_TREE); 
                    /* next find the definition */
                    slot = (map_d_p *) htab_find_slot (ref_def_mappings, entry, NO_INSERT);
                    if (slot) 
                      matched = match_one_symbol ((*slot)->dest);
                    else 
                      PUSH_TREE_CELL (node, pck_var);
                  }
                else 
                  PUSH_TREE_CELL (node, pck_var);
              }
            else if (stmt && TREE_CODE (stmt) != NOP_EXPR)
              matched = match_one_symbol (stmt); 
            else  
              PUSH_TREE_CELL (node, pck_semifun);
            if (matched == 0 || matched == 2 || matched == 3)
              return matched;

            break; 
          }

        case PHI_NODE:
          /* TOdo: should have no such nodes, since we handle it in the
             GIMPLE_MODIFY_TREE. */
          break;

        default:
          break;
        }
    default:
      break;
    }
  return 1;
}

#undef PUSH_TREE_CELL

/* Initialize the data for creating patterns */

static unsigned int init_pattern (void)
{
  if (middle_end_rules_table)
    {
      axiom_expr_free (middle_end_rules_table);
      middle_end_rules_table = NULL;
    }
     
  return 0;
}

/* position operation */

static char* calc_cell_position (const char* ancestor, int index)
{
  char *buf;
  char tmp[5];
  int len;

  if (!ancestor)
    {
      buf = (char *) xmalloc (5);
      strcpy (buf, "1");
      return buf;
    }

  len = strlen (ancestor);
  buf = (char *) xmalloc (len + 5); 

  strcpy (buf, ancestor);
  strcat (buf, ".");
  sprintf (tmp, "%d", index);
  strcat (buf, tmp);
    
  return buf;
}

/* Precedence Table for the pattern cell */
/* pck_none pck_code pck_parm pck_var pck_const pck_fun pck_tree */

static const unsigned int pattern_cell_relation[8][8] = 
  {
    /*                none const var  ref  fun semi code */
    /* pck_none  */    {0,   0,   0,   0,   0,   0,   0},
    /* pck_const  */   {0,   1,   0,   0,   0,   0,   0},
    /* pck_var  */     {0,   1,   1,   1,   1,   1,   1},
    /* pck_ref   */    {0,   0,   0,   1,   0,   0,   0},
    /* pck_fun */      {0,   0,   0,   0,   1,   0,   0},
    /* pck_semifun  */ {0,   0,   0,   0,   0,   1,   0},
    /* pck_code   */   {0,   0,   0,   0,   0,   0,   1},
  };

/* return value:
 *  0 stands for complete match 
 *  1 stands for position mismatch
 *  2 stands for first one greater position 
 *  3 stands for type mismatch 
 *  4 stands for kind mismatch 
 *  5 stands for address or code mismatch */
static unsigned int
is_pattern_cell_match (pattern_cell_ptr first, pattern_cell_ptr second)
{
  int len;
  int ret;
  tree type1;
  tree type2;

  gcc_assert (first);
  gcc_assert (second);

  /* kind -- kind */
  if (pattern_cell_relation[first->kind][second->kind] != 1) 
	return 0;

  /* type -- type */
  if ((first->type && !second->type)|| (!first->type && second->type))
    return 0;


  /* Const -- Const */
  if (first->kind == pck_const && second->kind == pck_const)
    {
      if (first->code != second->code) return 0;

      switch (first->code)
        {
        case INTEGER_CST:
          if (TREE_INT_CST_HIGH (first->node) != TREE_INT_CST_HIGH (second->node) ||
              TREE_INT_CST_LOW (first->node) != TREE_INT_CST_LOW (second->node))
            return 0;
          break;

        case REAL_CST:
          {
            if (!real_identical(TREE_REAL_CST_PTR (first->node),
                                TREE_REAL_CST_PTR (second->node)))
              return 0; 
          }
          break;

        case VECTOR_CST:
          {
            /* Todo: Not sure how to do that, could be done here or when
               creating pattern */
            /* Never be this since we handle it as a fun */
          }
          break;

        case COMPLEX_CST:
          /* Never be this since we handle it as a fun */
          break;

        case STRING_CST:
          {
            /* need to compare the length and the value */
            const char *p = TREE_STRING_POINTER (first->node);
            const char *q = TREE_STRING_POINTER (second->node);
            int i = TREE_STRING_LENGTH (first->node);
            int j = TREE_STRING_LENGTH (second->node);

            if (i != j || strcmp (p, q)) return 0;
          }
          break;
        default:
          break;
        }
    }
  else if (first->kind == pck_ref && second->kind == pck_ref && 
           first->code != second->code)
    return 0;
  else if (first->kind == pck_fun && second->kind == pck_fun &&
           first->code != second->code)
    return 0;
  else if (first->kind == pck_semifun && second->kind == pck_semifun &&
           first->code != second->code)
    return 0;
  else if (first->kind == pck_code && second->kind == pck_code) 
    {
      if (first->code != second->code) return 0;

      if (first->code == IDENTIFIER_NODE &&
          first->node != second->node) return 0;
      else if (TREE_CODE_CLASS (first->code) == tcc_type &&
               first->node != second->node) return 0;

      if ((TREE_CODE_CLASS (first->code) == tcc_declaration ||
          TREE_CODE_CLASS (first->code) == tcc_type) &&
          first->node != second->node)
        return 0;
    }

  /* compare types */
  type1 = TYPE_MAIN_VARIANT (first->type);
  type2 = TYPE_MAIN_VARIANT (second->type);
  while (POINTER_TYPE_P (type1) || TREE_CODE (type1) == ARRAY_TYPE)
    type1 = TYPE_MAIN_VARIANT (TREE_TYPE (type1));
  while (POINTER_TYPE_P (type2) || TREE_CODE (type2) == ARRAY_TYPE)
    type2 = TYPE_MAIN_VARIANT (TREE_TYPE (type2));

  if (!lang_hooks.check_node_info_2 (type1, type2, 1))
    return 0;

  len = strlen (current_cell->position);

  if (!current_cell->chain) /* reach the end */
    ret = 3;
  else if (strncmp (current_cell->position, current_cell->chain->position, len)) 
    ret = 2;
  else 
    ret = 1;

  if (first->kind == pck_var) 
    {
      VEC_safe_push (pattern_cell_ptr, heap, pattern_parms_stack, first);
      VEC_safe_push (pattern_cell_ptr, heap, subject_arguments_stack, second);
    }

  current_cell = current_cell->chain;

  return ret;
}

static unsigned int
is_pattern_cell_match_1 (pattern_cell_ptr first, pattern_cell_ptr second)
{
  tree type1, type2;

  gcc_assert (first);
  gcc_assert (second);

  /* kind -- kind */
  if (first->kind != second->kind) 
	return 0;

  /* type -- type */
  if ((first->type && !second->type)|| (!first->type && second->type))
    return 0;

  /* Const -- Const */
  if (first->kind == pck_const && second->kind == pck_const)
    {
      if (first->code != second->code) return 0;

      switch (first->code)
        {
        case INTEGER_CST:
          if (TREE_INT_CST_HIGH (first->node) != TREE_INT_CST_HIGH (second->node) ||
              TREE_INT_CST_LOW (first->node) != TREE_INT_CST_LOW (second->node))
            return 0;
          break;

        case REAL_CST:
          {
            if (!real_identical(TREE_REAL_CST_PTR (first->node),
                                TREE_REAL_CST_PTR (second->node)))
              return 0; 
          }
          break;

        case VECTOR_CST:
          {
            /* Todo: Not sure how to do that, could be done here or when
               creating pattern */
            /* Never be this since we handle it as a fun */
          }
          break;

        case COMPLEX_CST:
          /* Never be this since we handle it as a fun */
          break;

        case STRING_CST:
          {
            /* need to compare the length and the value */
            const char *p = TREE_STRING_POINTER (first->node);
            const char *q = TREE_STRING_POINTER (second->node);
            int i = TREE_STRING_LENGTH (first->node);
            int j = TREE_STRING_LENGTH (second->node);

            if (i != j || !strcmp (p, q)) return 0;
          }
          break;
        default:
          break;
        }
    }
  else if (first->kind == pck_ref && second->kind == pck_ref && 
           first->code != second->code)
    return 0;
  else if (first->kind == pck_fun && second->kind == pck_fun &&
           first->code != second->code)
    return 0;
  else if (first->kind == pck_semifun && second->kind == pck_semifun &&
           first->code != second->code)
    return 0;
  else if (first->kind == pck_code && second->kind == pck_code) 
    {
      if (first->code != second->code) return 0;

      if (first->code == IDENTIFIER_NODE &&
          first->node != second->node) return 0;
      else if (TREE_CODE_CLASS (first->code) == tcc_type &&
               first->node != second->node) return 0;

      if ((TREE_CODE_CLASS (first->code) == tcc_declaration ||
          TREE_CODE_CLASS (first->code) == tcc_type) &&
          first->node != second->node)
        return 0;
    }

  /* compare types */
  type1 = TYPE_MAIN_VARIANT (first->type);
  type2 = TYPE_MAIN_VARIANT (second->type);
  while (POINTER_TYPE_P (type1) || TREE_CODE (type1) == ARRAY_TYPE)
    type1 = TYPE_MAIN_VARIANT (TREE_TYPE (type1));
  while (POINTER_TYPE_P (type2) || TREE_CODE (type2) == ARRAY_TYPE)
    type2 = TYPE_MAIN_VARIANT (TREE_TYPE (type2));

  if (!lang_hooks.check_node_info_2 (type1, type2, 1))
    return 0;

  if (first->kind == pck_var && second->kind == pck_var &&
      first->node != second->node)
    return 0;

  return 1;
}

/* Unification */

static bool solve_equations (void) 
{
  pattern_cell_ptr var, value;
  pattern_cell_ptr var1, value1;
  unsigned ix, jx;
  tree t1, t2;

  for (ix = 0; 
       VEC_iterate (pattern_cell_ptr, pattern_parms_stack, ix, var),
         VEC_iterate (pattern_cell_ptr, subject_arguments_stack, ix, value);
       ix++)
    {
      for (jx = ix + 1; 
           VEC_iterate (pattern_cell_ptr, pattern_parms_stack, jx, var1),
             VEC_iterate (pattern_cell_ptr, subject_arguments_stack, jx, value1);
           jx++)
        {
          /* Here we exclude some complex cases: &&&a */
          if (is_pattern_cell_match_1 (var, var1) &&
              is_pattern_cell_match_1 (value, value1))
            {
              /* figure out the meaning node */
              t1 = value->node;
              t2 = value->node;
              while (TREE_CODE (t1) == ADDR_EXPR) 
                  t1 = TREE_OPERAND (t1, 0);
              while (TREE_CODE (t2) == ADDR_EXPR) 
                  t2 = TREE_OPERAND (t2, 0);
              if (t1 != t2)
                return false;
            }
        }
    }
  return true;
}

/* When inlining shall be performed. */

static bool 
gate_axiom_optimizer (void)
{
  /* set for generic functions only */
  return flag_inline_axiom_function;
}

static bool 
gate_axiom_match_optimizer (void)
{
  /* set for generic functions only */
  optimize_generic_function_p = 1;
  return flag_inline_axiom_function;
}

static bool 
gate_axiom_match_optimizer_1 (void)
{
  /* set for generic functions only */
  optimize_generic_function_p = 0;
  return flag_inline_axiom_function;
}

/* Executor to create pattern */

static unsigned int 
execute_add_pattern (void) 
{
  struct cgraph_node *node;
  tree saved = current_function_decl;
  char buffer[8];
  char name[32];

  strcpy (name, "AXIOMPATTERNS");

  sprintf (buffer, "%u", local_dump_no++);
  strcat (name, buffer);
  strcat (name, ".log");
  if (!dump_file)
    dump_file = fopen (name, "w"); 

  for (node = cgraph_nodes; node; node = node->next)
    {
      push_cfun (DECL_STRUCT_FUNCTION (node->decl));
      current_function_decl = node->decl;
      free_dominance_info (CDI_DOMINATORS);
      free_dominance_info (CDI_POST_DOMINATORS);
      add_pattern ();
      pop_cfun ();
      ggc_collect ();
    }

    fclose (dump_file);

  current_function_decl = saved;
  return 0;
}

/* Executor to do pattern match */
#if 0
static unsigned int 
execute_do_pattern_match (void) 
{
  struct cgraph_node *node;
  tree saved = current_function_decl;
  char buffer[8];
  char name[32];

  strcpy (name, "AXIOMMATCH");

  sprintf (buffer, "%u", local_dump_no);
  strcat (name, buffer);
  strcat (name, ".log");
  if (dump_file)
    dump_file = fopen (name, "w"); 

  for (node = cgraph_nodes; node; node = node->next)
    {
      push_cfun (DECL_STRUCT_FUNCTION (node->decl));
      current_function_decl = node->decl;
      free_dominance_info (CDI_DOMINATORS);
      free_dominance_info (CDI_POST_DOMINATORS);
      do_pattern_match();
      pop_cfun ();
      ggc_collect ();
    }

  fclose (dump_file);

  current_function_decl = saved;
  return 0;
}
#endif
struct tree_opt_pass pass_axiom_init = 
  {
    "axiominit",                                 /* name */
    gate_axiom_optimizer,            /* gate */
    init_pattern,                         /* execute */
    NULL,                                 /* sub */
    NULL,                                 /* next */
    0,                                    /* static_pass_number */
    0,                                    /* tv_id */
    PROP_cfg | PROP_ssa,                             /* properties_required */
    0,                                    /* properties_provided */
    0,                                    /* properties_destroyed */
    0,                                    /* todo_flags_start */
    0,                     /* todo_flags_finish */
    0					/* letter */
  };

struct tree_opt_pass pass_axiom_createpattern = 
  {
    "axiompattern",                                 /* name */
    gate_axiom_optimizer,            /* gate */
    execute_add_pattern,                         /* execute */
    NULL,                                 /* sub */
    NULL,                                 /* next */
    0,                                    /* static_pass_number */
    0,                                    /* tv_id */
    PROP_cfg | PROP_ssa,                             /* properties_required */
    0,                                    /* properties_provided */
    0,                                    /* properties_destroyed */
    0,                                    /* todo_flags_start */
    0,                     /* todo_flags_finish */
    0					/* letter */
  };

struct tree_opt_pass pass_axiom_patternmatch = 
  {
    "axiommatch",                                 /* name */
    gate_axiom_match_optimizer,            /* gate */
    do_pattern_match,                    /* execute */
    NULL,                                 /* sub */
    NULL,                                 /* next */
    0,                                    /* static_pass_number */
    0,                                    /* tv_id */
    PROP_cfg | PROP_ssa,                             /* properties_required */
    0,                                    /* properties_provided */
    0,                                    /* properties_destroyed */
    0,
    TODO_dump_func |                    /* todo_flags_finish */
    TODO_verify_all |
    TODO_update_ssa,
    0					/* letter */
  };

struct tree_opt_pass pass_axiom_patternmatch_1 = 
  {
    "axiommatch1",                                 /* name */
    gate_axiom_match_optimizer_1,            /* gate */
    do_pattern_match,                    /* execute */
    NULL,                                 /* sub */
    NULL,                                 /* next */
    0,                                    /* static_pass_number */
    0,                                    /* tv_id */
    PROP_cfg | PROP_ssa,                             /* properties_required */
    0,                                    /* properties_provided */
    0,                                    /* properties_destroyed */
    0,
    TODO_dump_func |                    /* todo_flags_finish */
    TODO_verify_all |
    TODO_update_ssa,
    0					/* letter */
  };

/* Print out the whole table */

static void print_rewrite_table (void)
{
  int a, b;
  axiom_expr_ptr p;

  if (!middle_end_rules_table) 
    {
      fprintf (dump_file, "%s", "\nnull middle end table\n");
      return;
    }

  fprintf (dump_file, "\n----------------begin----------------------");
  fprintf (dump_file, "\nThe middle end table is as follows:\n");
 
  for (p = middle_end_rules_table; p; p = p->chain)
    {
      a = 1; 
      b = 1;
      sum_row_and_col (p->lhs->chain, &a, &b, a, b);
    }

  fprintf (dump_file, "-------------------end----------------------\n");
}

/* Print out the inscope table */

static void print_rewrite_table_inscope (void)
{
  int a, b;
  axiom_expr_ptr p;

  fprintf (dump_file, "\n----------------begin----------------------");
  fprintf (dump_file, "\nThe inscope table is as follows:\n");
 
  for (p = rules_table_inscope; p; p = p->next)
    {
      a = 1; 
      b = 1;
      sum_row_and_col (p->lhs->chain, &a, &b, a, b);
    }

  fprintf (dump_file, "-------------------end----------------------\n");
}

static void print_pattern_expr (pattern_cell_ptr p)
{
  int a, b;
  if (!p) 
    {
      fprintf (dump_file, "%s", "\nnull expression\n");
      return;
    }

  fprintf (dump_file, "\n----------------begin----------------------");
  fprintf (dump_file, "\nThe subject term is as follows:\n");
  a = 1; 
  b = 1;
  sum_row_and_col (p->chain, &a, &b, a, b);
  fprintf (dump_file, "-------------------end----------------------\n");
}

/* Print each item */

static void print_pattern_cell (pattern_cell_ptr cell)
{
  if (!cell) return;

  fprintf (dump_file, "\t[");
  fprintf (dump_file, "postion: \t%s", cell->position);
  if (cell->position && strlen (cell->position) < 9)
    fprintf (dump_file, "\t");		

  fprintf (dump_file, "\t");		
  fprintf (dump_file, "code\t%s", tree_code_name [(unsigned) cell->code]);    

  fprintf (dump_file, "\t");		
  fprintf (dump_file, "type:\t%x", (unsigned) cell->type);

  fprintf (dump_file, "\t");		
  fprintf (dump_file, "node\t%x", (unsigned) cell->node);     

  fprintf (dump_file, "\t");		
  switch (cell->kind)
    {
    case pck_none:
      fprintf (dump_file, "pck_none_cell");
      break;
    case pck_const: 
      fprintf (dump_file, "pck_const_cell");
      break;
    case pck_var:
      fprintf (dump_file, "pck_var_cell");
      break;
    case pck_ref:
      fprintf (dump_file, "pck_ref_cell");
      break;
    case pck_fun:
      fprintf (dump_file, "pck_fun_cell");
      break;
    case pck_semifun:
      fprintf (dump_file, "pck_semifun_cell");      
      break;
    case pck_code:
      fprintf (dump_file, "pck_code_cell");
      break;
    default:
      fprintf (dump_file, "unknown_cell");
    }

  fprintf (dump_file, "]\n");
}

/* Computing row and column */

static void 
sum_row_and_col (pattern_cell_ptr head, 
                 int *row, 
                 int *col, 
                 int startx, 
                 int starty) 
{
  int z = starty;
  pattern_cell_ptr tmp_cell;
  if (!head)
    {
      fprintf (dump_file, "\n");
      *row = *row + 1;
      return;
    }
  /* print out the coordinate for the cell and the value */
  startx = *row;
  fprintf (dump_file, "(%d,%d)", *row, starty);
  print_pattern_cell (head);

  sum_row_and_col (head->chain, row, col, startx, starty + 1);

  tmp_cell = head->sibling;
  while (tmp_cell)
    {
      fprintf (dump_file, "(%d, %d)", *row, z);
      print_pattern_cell (tmp_cell);
      sum_row_and_col (tmp_cell->chain, row, col, startx, z + 1);
      tmp_cell = tmp_cell->sibling;
    }
}

/* Print parameters stack */
static void
print_equations (void)
{
  pattern_cell_ptr var, value;
  unsigned ix;

  for (ix = 0; 
       VEC_iterate (pattern_cell_ptr, pattern_parms_stack, ix, var),
         VEC_iterate (pattern_cell_ptr, subject_arguments_stack, ix, value);
       ix++)
    {
      fprintf (dump_file, "\n Meta varible in the pattern:\t"); 
      print_pattern_cell (var);
      fprintf (dump_file, "\n Varible in the source code:\t"); 
      print_pattern_cell (value);
    }
}

