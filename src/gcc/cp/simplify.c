/* Concept-dependent simplifier for parse phase of GNU compiler.
   Copyright (C) 2006 Free Software Foundation, Inc.
   Hacked by Texas A&M University

   This file is part of GCC.

   GCC is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   GCC is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GCC; see the file COPYING.  If not, write to
   the Free Software Foundation, 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* The file contains the code to handle axioms: splitting axioms into
   small function units, orgnizing them into rewrite repository, and
   performing transformations in the front-end. */
/* We are going to move all of the rewriting code into a common
   rewrite module, so that the front-end and the middle-end could
   share it. */

#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "tm.h"
#include "tree.h"
#include "cp-tree.h"
#include "flags.h"
#include "toplev.h"
#include "rtl.h"
#include "diagnostic.h"
#include "hashtab.h"
#include "tree-iterator.h"
#include "tree-flow.h"
#include "diagnostic.h"
#include "cxx-pretty-print.h"
#include "tree-inline.h"

typedef tree (*walk_tree_fn_1) (tree *, tree *, int*);
typedef struct axiom_expr *axiom_expr_ptr;
typedef struct pattern_cell *pattern_cell_ptr;

/* axiom entry */
struct axiom_expr 
{
    pattern_cell_ptr cell_p;
    tree template;
    axiom_expr_ptr chain; 
};

/* a free list of "axiom_expr"s awaiting for re-use */
static GTY((deletable)) axiom_expr_ptr free_axiom_expr = NULL;

/* pattern cell kind */
enum pattern_cell_kind 
{
    pck_none  = 0,
    pck_code  = 1,
    pck_parm  = 2,
    pck_var   = 3,
    pck_const = 4,
    pck_ref   = 5,
    pck_fun   = 6,
    pck_exp   = 7
};

/* pattern cell union*/
struct pattern_cell_u
{
    tree decl;
    enum tree_code code;
};

/* pattern cell */
struct pattern_cell 
{
    /* cell position */
    char *position;
    enum pattern_cell_kind kind;
    struct pattern_cell_u u; 
    tree type;
    tree* orig_addr;
    pattern_cell_ptr sibling;
    pattern_cell_ptr next;
    pattern_cell_ptr chain;
};

/* pattern table derived from axioms */
static axiom_expr_ptr axiom_expr_pattern_table = NULL;

/* a free list of "pattern_cell"s awaiting for re-use */
static GTY((deletable)) pattern_cell_ptr free_pattern_cell = NULL;

/* track the record cell */
static pattern_cell_ptr current_cell = NULL;

DEF_VEC_P (pattern_cell_ptr);
DEF_VEC_ALLOC_P (pattern_cell_ptr, gc);

/* parameter stacks for the pattern */
static VEC(pattern_cell_ptr, gc) *pattern_parms_stack = NULL;

/* corresponding arguments for the subject term */ 
static VEC(pattern_cell_ptr, gc) *subject_arguments_stack = NULL;

typedef struct ptr_to_cell *ptr_to_cell_ptr;
struct ptr_to_cell 
{
  tree *ptr;
  pattern_cell_ptr cell;
  ptr_to_cell_ptr chain;
};

/* hash map between address and current_cell */
static htab_t ptr_cell_mappings;
static ptr_to_cell_ptr free_ptr_to_cell_list = NULL;

/* function forward declaration */
static pattern_cell_ptr pattern_cell_make (void);
static pattern_cell_ptr pattern_cell_lookup_and_create (pattern_cell_ptr,
                                                        enum pattern_cell_kind,
                                                        struct pattern_cell_u,
                                                        char*,
                                                        tree,
                                                        tree*);
static void construct_pattern_table_per_stmt (tree*);
static tree create_pattern_for_node (tree*, tree*, int*);
static tree walk_tree_with_parent (tree*, tree*, walk_tree_fn_1);
static tree cp_walk_subtrees_with_parent (tree*, int*, walk_tree_fn_1);
static void print_pattern_table (void);
static void print_pattern_expr (pattern_cell_ptr);
static void print_pattern_cell (pattern_cell_ptr);
static void sum_row_and_col (pattern_cell_ptr, int*, int*, int, int);
static hashval_t hash_ptr_to_cell (const void *p);
static int eq_ptr_to_cell (const void *p1, const void *p2);
static ptr_to_cell_ptr make_ptr_to_cell (tree *); 
static void free_ptr_to_cell (void *);

static char* calc_cell_position (const char* ancestor, int);
static char* calc_cell_position_for_sibling (const char*);
static void assign_pos_to_cell (pattern_cell_ptr, char*);
static unsigned 
is_pattern_cell_match (pattern_cell_ptr, pattern_cell_ptr);
static void transform_expr_with_concept_axiom (pattern_cell_ptr);
static bool solve_equations (void);
static int index_of_pattern_parms (tree);
static tree replace_parm_with_arg (tree *, int *, void *);
static void 
walk_cell_bfs (axiom_expr_ptr, pattern_cell_ptr, const char*);
static bool 
match_and_transform (pattern_cell_ptr, pattern_cell_ptr, const char*);

static tree build_rules_per_axiom (tree);
static tree build_function_decl_per_pattern (tree, tree, tree);

/* All axioms are prepared in two stages: the concept level and the
   axiom level. The concept level is repsonsible for linking up all
   rewriting rules generated from axioms. The axiom level is
   responsible for linking up all rewriting rules generated from each
   equation defined in each axiom. */

static int counter = 0;
static char prefix[10] = "__axiom__";

/* The function is going to build a new function decl according to the
   given function and function body.

   AXIOM is the function_decl and BODY is to be the function body.
   TYPE is the type for the function to be built. */

static tree
build_function_decl_per_pattern (tree axiom, tree type, tree body) 
{
  tree map = DECL_CONTEXT (axiom);
  tree func;
  tree identifier;
  char id[20];
  char axiomno[10];
  tree stmt, compount_stmt;
  tree resdecl;

  gcc_assert (CLASSTYPE_MODEL_P (map));

  /* Build the declaration. */
  func = copy_decl (axiom);
  
  /* Set the function type */
  TREE_TYPE (func) = type; 

  /* Create the identifier. */
  strcpy (id, prefix);
  sprintf (axiomno, "%u", ++counter);
  strcat (id, axiomno);
  /* Get the identifier. */
  identifier = get_identifier (id);
  /* Set the identifer. */
  DECL_NAME (func) = identifier;

  /* Clear function body */
  DECL_SAVED_TREE (func) = NULL_TREE;
  TREE_CHAIN (func) = NULL_TREE;
  DECL_ARTIFICIAL (func) = 0;
  DECL_INITIAL (func) = NULL_TREE;

  /* Set the DECL_RESULT */
  resdecl = build_decl (RESULT_DECL, NULL_TREE, TREE_TYPE (type));
  DECL_ARTIFICIAL (resdecl) = 1;
  DECL_IGNORED_P (resdecl) = 1;
  DECL_RESULT (func) = resdecl;

  push_deferring_access_checks (dk_no_deferred);
  start_preparsed_function (func, NULL_TREE, 
                            SF_PRE_PARSED | SF_INCLASS_INLINE);

  stmt = begin_function_body ();
  compount_stmt = begin_compound_stmt (0);
  
  if (TREE_TYPE (type) != void_type_node)
    finish_return_stmt (body);
  else
    finish_expr_stmt (body);

  /* Finish the function body.  */
  finish_compound_stmt (compount_stmt);
  finish_function_body (stmt);

  /* Generate code for the function, if necessary.  */
  expand_or_defer_fn (finish_function (/*inclass_inline*/2));
  pop_deferring_access_checks ();

  /* Set the chain */
  TREE_CHAIN (func) = CLASSTYPE_AXIOM_METHODS (map);
  CLASSTYPE_AXIOM_METHODS (map) = func;
  return func;
}

/* The function is responsible for recognizing all equations in one
   axiom. */

static tree 
build_rules_per_axiom (tree axiom) 
{
  tree equation;
  tree left;
  tree right;
  tree result = NULL_TREE;
  /* tree conditona; */
  tree_stmt_iterator i;
  bool keep_p = true;
  tree type;
  tree body = DECL_SAVED_TREE (axiom);

  fprintf (stdout, "\n----------------------------------------------");

  while (keep_p) {
    switch (TREE_CODE (body))
      {
      case EXPR_STMT:
        body = EXPR_STMT_EXPR (body);
        break;
      case BIND_EXPR: 
        body = BIND_EXPR_BODY (body);
        break;
      case CLEANUP_POINT_EXPR:
        body = TREE_OPERAND (body, 0);
        break;
      case CONVERT_EXPR:
        body = TREE_OPERAND (body, 0);
        break;
      default:
        keep_p = false;
        break;
      }
  }
  /* already reach an EQ_EXPR */
  if (TREE_CODE (body) == EQ_EXPR) 
    {
      equation = body;

      /* announce axioms */
      fprintf (stdout, "\nStaring to process one equation in the axiom:");
      print_node_brief (stdout, "Debugging axioms", axiom, 0);
      fprintf (stdout, "\nThe equation:");
      print_node_brief (stdout, "Debugging axioms", equation, 4);
      fprintf (stdout, "\nFinishing to process one equation in the axiom.\n");
      
      left = TREE_OPERAND (equation, 0);
      right = TREE_OPERAND (equation, 1);
      if (!left || !right)
        return NULL_TREE;

      /* Build the new function type */
      type = copy_type (TREE_TYPE (axiom));
      TREE_TYPE (type) = TREE_TYPE (right);
      right = build_function_decl_per_pattern (axiom, type, right);
      FUNCTION_AXIOM_TEMPLATE_P (right) = 1;

      type = copy_type (TREE_TYPE (axiom));
      TREE_TYPE (type) = TREE_TYPE (left);
      left = build_function_decl_per_pattern (axiom, type, left);
      FUNCTION_AXIOM_PATTERN_P (left) = 1;

      /* combine them into a tree node */
      if (!result) result = build_tree_list (left, right);
      else chainon (result, build_tree_list (left, right));

      if (!result) return NULL_TREE;
      return build_tree_list (axiom, result);
   }

  if (TREE_CODE (body) != STATEMENT_LIST) 
    return NULL_TREE;

  fprintf (stdout, "\nStaring to process statements in one axiom one-by-one!");

  /* we should have found the STMT_LIST */
  for (i = tsi_start (body); !tsi_end_p (i); tsi_next (&i))
    {
      equation = tsi_stmt (i);
      keep_p = true;
      while (keep_p) {
        switch (TREE_CODE (equation))
          {
          case EXPR_STMT:
            equation = EXPR_STMT_EXPR (equation);
            break;
          case BIND_EXPR: 
            equation = BIND_EXPR_BODY (equation);
            break;
          case CLEANUP_POINT_EXPR:
            equation = TREE_OPERAND (equation, 0);
            break;
          case CONVERT_EXPR:
            equation = TREE_OPERAND (equation, 0);
            break;
          default:
            keep_p = false;
            break;
          }
      }
      if (TREE_CODE (equation) != EQ_EXPR) continue;

      /* announce axioms */
      fprintf (stdout, "\nStaring to process one equation in the axiom:");
      print_node_brief (stdout, "Debugging axioms", axiom, 0);
      fprintf (stdout, "\nThe equation:");
      print_node_brief (stdout, "Debugging axioms", equation, 4);
      fprintf (stdout, "\nFinishing to process one equation in the axiom.\n");
      
      left = TREE_OPERAND (equation, 0);
      right = TREE_OPERAND (equation, 1);
      if (!left || !right)
        return NULL_TREE;

      /* Build the new function type */
      type = copy_type (TREE_TYPE (axiom));
      TREE_TYPE (type) = TREE_TYPE (right);
      right = build_function_decl_per_pattern (axiom, type, right);
      FUNCTION_AXIOM_TEMPLATE_P (right) = 1;

      type = copy_type (TREE_TYPE (axiom));
      TREE_TYPE (type) = TREE_TYPE (left);
      left = build_function_decl_per_pattern (axiom, type, left);
      FUNCTION_AXIOM_PATTERN_P (left) = 1;
      /* combine them into a tree node */
      if (!result) result = build_tree_list (left, right);
      else chainon (result, build_tree_list (left, right));
    }

  if (!result) return NULL_TREE;
  return build_tree_list (axiom, result);
}

/* The function is responsible for linking up all rewrite rules built
   up by parser_axiom.

   MAP represents the concept_map to be analyzed. */

void 
build_rules_per_concept_map (tree map)
{
  tree func;
  tree result = NULL_TREE;
  tree rule;
  
  for (func = TYPE_METHODS (map); func; func = TREE_CHAIN (func))
    {
      if (TREE_CODE (func) == FUNCTION_DECL
          && (TREE_TYPE (TREE_TYPE (func)) == axiom_type_node))
        {
          gcc_assert (DECL_SAVED_TREE (func));
          rule = build_rules_per_axiom (func);
          if (!rule) continue;
          if (!result) result = rule;
          else chainon (result, rule);
        }
    }

  if (!result) return;

  result = build_tree_list (map, result);

  /* rule should be a TREE_LIST */
  if (!global_axiom_repository) global_axiom_repository = result;
  else chainon (global_axiom_repository, result);
}

/* calculate the hash code */
static hashval_t hash_ptr_to_cell (const void *p)
{
    const ptr_to_cell_ptr d = (ptr_to_cell_ptr) p;
    return htab_hash_pointer (d->ptr);
}

static int eq_ptr_to_cell (const void *p1, const void *p2)
{
    const ptr_to_cell_ptr d = (ptr_to_cell_ptr) p1;
    const ptr_to_cell_ptr s = (ptr_to_cell_ptr) p2;

    return (d->ptr == s->ptr);
}

/* create an fun_arg_map entry */
static ptr_to_cell_ptr make_ptr_to_cell (tree *ptr)
{
    ptr_to_cell_ptr entry;

    if (free_ptr_to_cell_list)
    {
	entry = free_ptr_to_cell_list;
	free_ptr_to_cell_list = entry->chain;
    }
    else
	entry = GGC_NEW (struct ptr_to_cell);

    entry->chain = NULL;
    entry->ptr = ptr;
    entry->cell = NULL;
    return entry;
}

/* put enry back on the free list */
static void free_ptr_to_cell (void *p)
{
    const ptr_to_cell_ptr entry = (ptr_to_cell_ptr) p;
    entry->chain = free_ptr_to_cell_list;
    free_ptr_to_cell_list = entry;
}


/* Create an axiom_cell object.  */
static pattern_cell_ptr pattern_cell_make (void)
{
    pattern_cell_ptr entry;

    if (free_pattern_cell)
    {
	entry = free_pattern_cell;
	free_pattern_cell = entry->chain;
    }
    else
	entry = GGC_NEW (struct pattern_cell);

    entry->kind = pck_none;
    entry->sibling = NULL;
    entry->position = NULL;
    entry->type = NULL;
    entry->next = NULL;
    entry->chain = NULL;
    entry->orig_addr = NULL;
    return entry;
}

/* lookup cell and create if not */
static pattern_cell_ptr 
pattern_cell_lookup_and_create (pattern_cell_ptr entry, 
	enum pattern_cell_kind class, 
	struct pattern_cell_u content, 
	char *position, 
	tree type,
	tree *orig_addr)
{  
    pattern_cell_ptr child, brother;
    gcc_assert (entry);
    child = entry->next;

    if (!child) 
    {
	/* create a new one to be the child of the axiom_cell_p */
	entry->next = pattern_cell_make ();
	entry->next->u.code = content.code;
	entry->next->u.decl = content.decl;
	entry->next->kind = class;
	entry->next->position = position;
	entry->next->type = type;
	entry->next->orig_addr = orig_addr;
	return entry->next;
    }

    for (brother = child->sibling; brother;
         child = brother, brother = brother->sibling)
      ;
    child->sibling = pattern_cell_make ();
    child->sibling->position = position;
    child->sibling->type = type;
    child->sibling->orig_addr = orig_addr;
	child->sibling->u.decl = content.decl;
	child->sibling->u.code = content.code;
    child->sibling->kind = class;
    return child->sibling;
}

/* create an axiom_expr object.  */
static axiom_expr_ptr axiom_expr_make (void)
{
    axiom_expr_ptr entry;

    if (free_axiom_expr)
    {
	entry = free_axiom_expr;
	free_axiom_expr = entry->chain;
    }
    else
	entry = GGC_NEW (struct axiom_expr);

    entry->cell_p = NULL;
    entry->template = NULL;
    entry->chain = NULL;

    return entry;
}

/* Put ENTRY back on the free list.  */
static void axiom_expr_free (axiom_expr_ptr entry)
{
  axiom_expr_ptr p;
  for (p = entry; p->chain; p = p->chain)
    ;
    p->chain = free_axiom_expr;
    free_axiom_expr = entry;
}

void init_concept_based_opt (void)
{
  axiom_expr_pattern_table = NULL;
}

#define RECUR(NODE)  \
construct_pattern_table_with_axioms(NODE); 

/* construct the pattern table from the axiom expressins */
void construct_pattern_table_with_axioms (tree *t)
{
    tree_stmt_iterator i;

    switch (TREE_CODE (*t)) 
      {
      case STATEMENT_LIST:
        for (i = tsi_start (*t); !tsi_end_p (i); tsi_next (&i))
          {
            RECUR (tsi_stmt_ptr (i));
          }
        break;
      case EXPR_STMT:
        RECUR (& EXPR_STMT_EXPR (*t));
        break;
      case BIND_EXPR: 
        RECUR (& BIND_EXPR_BODY (*t));
        break;
      case CLEANUP_POINT_EXPR:
        RECUR (& TREE_OPERAND (*t, 0));
        break;
      case CONVERT_EXPR:
        RECUR (& TREE_OPERAND (*t, 0));
        break;
      case EQ_EXPR:
        construct_pattern_table_per_stmt (t);
        break;
      default:
        return;
    }
}

#undef RECUR

static void 
construct_pattern_table_per_stmt (tree *tp)
{
  ptr_to_cell_ptr *slot = NULL;
  ptr_to_cell_ptr pelement;

  axiom_expr_ptr axiom;

  gcc_assert (TREE_CODE (*tp) == EQ_EXPR);

  /* create a new axiom and link it into the list */
  axiom = axiom_expr_make ();
  axiom->chain = axiom_expr_pattern_table;
  axiom_expr_pattern_table = axiom;
  
  axiom->template = TREE_OPERAND (*tp, 1);

  current_cell = pattern_cell_make ();
  axiom->cell_p = current_cell;

  ptr_cell_mappings = htab_create (50, hash_ptr_to_cell, eq_ptr_to_cell,
                                   free_ptr_to_cell);

  pelement = make_ptr_to_cell (tp);
  pelement->cell = current_cell;

  slot = (ptr_to_cell_ptr *) htab_find_slot (ptr_cell_mappings, pelement, INSERT);
  *slot = pelement;

  walk_tree_with_parent (&TREE_OPERAND (*tp, 0), tp, create_pattern_for_node);

  assign_pos_to_cell (axiom->cell_p->next, calc_cell_position (NULL, 1));
  
  print_pattern_table ();

  htab_delete (ptr_cell_mappings);
}
  
void cp_concept_optimizer_engine (tree *cand)
{
    ptr_to_cell_ptr pelement;  
    ptr_to_cell_ptr *slot = NULL;
    pattern_cell_ptr header;
  
    /* null expr, just return immediately */
    if (!*cand) return;
    
    /* no declared axiom yet, just skip the optimization */
    if (!axiom_expr_pattern_table) return;
	    
    header = pattern_cell_make ();
    current_cell = header;

    ptr_cell_mappings = htab_create (50, hash_ptr_to_cell, eq_ptr_to_cell,
                                   free_ptr_to_cell);

    pelement = make_ptr_to_cell (NULL);
    pelement->cell = current_cell;

    slot = (ptr_to_cell_ptr *) htab_find_slot (ptr_cell_mappings, pelement, INSERT);
    *slot = pelement;

    walk_tree_with_parent (cand, NULL, create_pattern_for_node);

    assign_pos_to_cell (header->next, calc_cell_position (NULL, 1));
    print_pattern_expr (header);

    /* next we are going to optimize */
    transform_expr_with_concept_axiom (header);

    /*
    htab_delete (ptr_cell_mappings);

    axiom_expr_free (axiom_expr_pattern_table);
    */

    fprintf (stdout, "The result is:\n");

    /* we output the result */
    header = pattern_cell_make ();
    current_cell = header;

    ptr_cell_mappings = htab_create (50, hash_ptr_to_cell, eq_ptr_to_cell,
                                   free_ptr_to_cell);

    pelement = make_ptr_to_cell (NULL);
    pelement->cell = current_cell;

    slot = (ptr_to_cell_ptr *) htab_find_slot (ptr_cell_mappings, pelement, INSERT);
    *slot = pelement;

    walk_tree_with_parent (cand, NULL, create_pattern_for_node);

    assign_pos_to_cell (header->next, calc_cell_position (NULL, 1));
    print_pattern_expr (header);

    htab_delete (ptr_cell_mappings);

    axiom_expr_free (axiom_expr_pattern_table);
    axiom_expr_pattern_table = NULL;
}

#define PUSH_TREE_CELL(NODE, DECL, KIND, ADDR)  \
{ \
content.code = TREE_CODE (NODE); \
content.decl = (DECL); \
current_cell = \
pattern_cell_lookup_and_create (current_cell, \
                                KIND, \
                                content, \
                                pos, \
                                TREE_TYPE (NODE), \
                                (ADDR));  \
pelement = make_ptr_to_cell (tp); \
pelement->cell = current_cell; \
slot = (ptr_to_cell_ptr *) htab_find_slot (ptr_cell_mappings, pelement, INSERT); \
*slot = pelement; } \

static tree 
create_pattern_for_node (tree *tp, tree *tpp, int *walk_subtrees)
{
  enum tree_code code;
  struct pattern_cell_u content;
  ptr_to_cell_ptr *slot = NULL;
  ptr_to_cell_ptr slot1; 
  char *pos = NULL;
  ptr_to_cell_ptr pelement;

  gcc_assert (*tp != NULL_TREE);
  gcc_assert (ptr_cell_mappings != NULL);

  pelement = make_ptr_to_cell (tp); 
  slot1 = (ptr_to_cell_ptr) htab_find (ptr_cell_mappings, pelement); 
  gcc_assert (slot1 == NULL); 

  pelement->ptr = tpp;
  slot1 = (ptr_to_cell_ptr) htab_find (ptr_cell_mappings, pelement); 
  gcc_assert (slot1 != NULL); 

  current_cell = slot1->cell;
  free_ptr_to_cell (pelement); 

  code  = TREE_CODE (*tp);

  switch (code) 
    {
    case INDIRECT_REF:
    case ARRAY_REF:
    case MEMBER_REF:
    case OFFSET_REF: /* todo: analyze COMPONENT_REF, SCOPE_REF */
      PUSH_TREE_CELL (*tp, NULL_TREE, pck_ref, tp)
      break;
    case NOP_EXPR:
    case CONVERT_EXPR: /* todo: ? only two conversions */
      PUSH_TREE_CELL (*tp, NULL_TREE, pck_exp, tp)
      break;
    case NEW_EXPR:
    case VEC_NEW_EXPR:
    case DELETE_EXPR:
    case VEC_DELETE_EXPR:
    case CALL_EXPR:
      PUSH_TREE_CELL (*tp, NULL_TREE, pck_fun, tp)
      break;

      /* The most simplified variables which are declared by users or the
         compiler itself, and the variable should be unique, and its
         properties are determined by its type. */
    case VAR_DECL: 
      PUSH_TREE_CELL (*tp, *tp, pck_var, tp) 
      *walk_subtrees = 0;
      break;

      /* It is similar to VAR_DECL, but appears inside the function
         definition, etc. */
    case PARM_DECL: 
      PUSH_TREE_CELL (*tp, *tp, pck_parm, tp) 
      *walk_subtrees = 0;
      break;

      /* It is similar to VAR_DECL, but its value is determined by
         DECL_INITIAL. */
    case CONST_DECL:
      PUSH_TREE_CELL (*tp, DECL_INITIAL (*tp), pck_const, tp); 
      *walk_subtrees = 0;
      break;

      /* Same name always means same id.
         todo: some kinds of nodes should never appear in the
         expession or statement context, could we figure them
         out ?
         so: Can we remove this part ? */
    case IDENTIFIER_NODE: 
    case ERROR_MARK:
      PUSH_TREE_CELL (*tp, *tp, pck_code, tp);
      *walk_subtrees = 0;
      break;

    default:
      /* If there are no operands any more, so its uniqueness is
         determined by itself instead of all of its operands. */

      /* For constant class node, assuming that they all are
         unique. In addition: totally 6 kinds, 5 in tree.def
         and 1 in cp_tree.def.
         INTEGER_CST; REAL_CST; COMPLEX_CST; VECTOR_CST;
         STRING_CST; PTRMEM_CST. */
      if (CONSTANT_CLASS_P (*tp))
        PUSH_TREE_CELL (*tp, *tp, pck_const, tp)
      else if (IS_TYPE_OR_DECL_P (*tp))
        {
          PUSH_TREE_CELL (*tp, *tp, pck_code, tp);
          *walk_subtrees = 0;
        }
      else if (REFERENCE_CLASS_P (*tp))
        PUSH_TREE_CELL (*tp, *tp, pck_ref, tp)
      else if (COMPARISON_CLASS_P (*tp))
        PUSH_TREE_CELL (*tp, *tp, pck_exp, tp)
      else if (UNARY_CLASS_P (*tp))
        PUSH_TREE_CELL (*tp, *tp, pck_exp, tp)
      else if (BINARY_CLASS_P (*tp))
        PUSH_TREE_CELL (*tp, *tp, pck_exp, tp)
      else if (EXPR_P (*tp))
        PUSH_TREE_CELL (*tp, *tp, pck_exp, tp)
      else /* reference and statement and exceptiional */
        PUSH_TREE_CELL (*tp, NULL_TREE, pck_code, tp)
    }

  return NULL_TREE;

}
#undef PUSH_TREE_CELL



#define WALK_SUBTREE(NODE)				\
  do							\
    {							\
      result = walk_tree_with_parent (&(NODE), tp, func);	\
      if (result)					\
	return result;					\
    }							\
  while (0)


/* Similar to walk_tree, but one more argument parent. */


/* Apply FUNC to all the sub-trees of TP in a pre-order traversal.  FUNC is
   called with the DATA and the address of each sub-tree.  If FUNC returns a
   non-NULL value, the traversal is stopped, and the value returned by FUNC
   is returned.  If PSET is non-NULL it is used to record the nodes visited,
   and to avoid visiting a node more than once.  */

static tree
walk_tree_with_parent  (tree *tp, tree *tpp, walk_tree_fn_1 func )
{
  enum tree_code code;
  int walk_subtrees;
  tree result;

#define WALK_SUBTREE_TAIL(NODE)				\
  do							\
    {							\
       tp = & (NODE);					\
       goto tail_recurse;				\
    }							\
  while (0)

 tail_recurse:
  /* Skip empty subtrees.  */
  if (!*tp)
    return NULL_TREE;

  /* Call the function.  */
  walk_subtrees = 1;
  result = (*func) (tp, tpp, &walk_subtrees);

  /* If we found something, return it.  */
  if (result)
    return result;

  code = TREE_CODE (*tp);

  /* Even if we didn't, FUNC may have decided that there was nothing
     interesting below this point in the tree.  */
  if (!walk_subtrees)
    {
      /* But we still need to check our siblings.  */
      if (code == TREE_LIST)
	WALK_SUBTREE_TAIL (TREE_CHAIN (*tp));
      else if (code == OMP_CLAUSE)
	WALK_SUBTREE_TAIL (OMP_CLAUSE_CHAIN (*tp));
      else
	return NULL_TREE;
    }

  result = cp_walk_subtrees_with_parent (tp, &walk_subtrees, func);
  if (result || !walk_subtrees)
    return result;

  switch (code)
    {
    case ERROR_MARK:
    case IDENTIFIER_NODE:
    case INTEGER_CST:
    case REAL_CST:
    case VECTOR_CST:
    case STRING_CST:
    case BLOCK:
    case PLACEHOLDER_EXPR:
    case SSA_NAME:
    case FIELD_DECL:
    case RESULT_DECL:
      /* None of these have subtrees other than those already walked
	 above.  */
      break;

    case TREE_LIST:
      WALK_SUBTREE (TREE_VALUE (*tp));
      WALK_SUBTREE_TAIL (TREE_CHAIN (*tp));
      break;

    case TREE_VEC:
      {
	int len = TREE_VEC_LENGTH (*tp);

	if (len == 0)
	  break;

	/* Walk all elements but the first.  */
	while (--len)
	  WALK_SUBTREE (TREE_VEC_ELT (*tp, len));

	/* Now walk the first one as a tail call.  */
	WALK_SUBTREE_TAIL (TREE_VEC_ELT (*tp, 0));
      }

    case COMPLEX_CST:
      WALK_SUBTREE (TREE_REALPART (*tp));
      WALK_SUBTREE_TAIL (TREE_IMAGPART (*tp));

    case CONSTRUCTOR:
      {
	unsigned HOST_WIDE_INT idx;
	constructor_elt *ce;

	for (idx = 0;
	     VEC_iterate(constructor_elt, CONSTRUCTOR_ELTS (*tp), idx, ce);
	     idx++)
	  WALK_SUBTREE (ce->value);
      }
      break;

    case SAVE_EXPR:
      tpp = tp;
      WALK_SUBTREE_TAIL (TREE_OPERAND (*tp, 0));

    case BIND_EXPR:
      {
	tree decl;
	for (decl = BIND_EXPR_VARS (*tp); decl; decl = TREE_CHAIN (decl))
	  {
	    /* Walk the DECL_INITIAL and DECL_SIZE.  We don't want to walk
	       into declarations that are just mentioned, rather than
	       declared; they don't really belong to this part of the tree.
	       And, we can see cycles: the initializer for a declaration
	       can refer to the declaration itself.  */
	    WALK_SUBTREE (DECL_INITIAL (decl));
	    WALK_SUBTREE (DECL_SIZE (decl));
	    WALK_SUBTREE (DECL_SIZE_UNIT (decl));
	  }
    tpp = tp;
	WALK_SUBTREE_TAIL (BIND_EXPR_BODY (*tp));
      }

    case STATEMENT_LIST:
      {
	tree_stmt_iterator i;
	for (i = tsi_start (*tp); !tsi_end_p (i); tsi_next (&i))
	  WALK_SUBTREE (*tsi_stmt_ptr (i));
      }
      break;

    case OMP_CLAUSE:
      switch (OMP_CLAUSE_CODE (*tp))
	{
	case OMP_CLAUSE_PRIVATE:
	case OMP_CLAUSE_SHARED:
	case OMP_CLAUSE_FIRSTPRIVATE:
	case OMP_CLAUSE_LASTPRIVATE:
	case OMP_CLAUSE_COPYIN:
	case OMP_CLAUSE_COPYPRIVATE:
	case OMP_CLAUSE_IF:
	case OMP_CLAUSE_NUM_THREADS:
	case OMP_CLAUSE_SCHEDULE:
	  WALK_SUBTREE (OMP_CLAUSE_OPERAND (*tp, 0));
	  /* FALLTHRU */

	case OMP_CLAUSE_NOWAIT:
	case OMP_CLAUSE_ORDERED:
	case OMP_CLAUSE_DEFAULT:
	  WALK_SUBTREE_TAIL (OMP_CLAUSE_CHAIN (*tp));

	case OMP_CLAUSE_REDUCTION:
	  {
	    int i;
	    for (i = 0; i < 4; i++)
	      WALK_SUBTREE (OMP_CLAUSE_OPERAND (*tp, i));
	    WALK_SUBTREE_TAIL (OMP_CLAUSE_CHAIN (*tp));
	  }

	default:
	  gcc_unreachable ();
	}
      break;

    case TARGET_EXPR:
      {
	int i, len;

	/* TARGET_EXPRs are peculiar: operands 1 and 3 can be the same.
	   But, we only want to walk once.  */
	len = (TREE_OPERAND (*tp, 3) == TREE_OPERAND (*tp, 1)) ? 2 : 3;
	for (i = 0; i < len; ++i)
	  WALK_SUBTREE (TREE_OPERAND (*tp, i));
    tpp = tp;
	WALK_SUBTREE_TAIL (TREE_OPERAND (*tp, len));
      }

    case DECL_EXPR:
      break;

    default:
      if (IS_EXPR_CODE_CLASS (TREE_CODE_CLASS (code))
	  || IS_GIMPLE_STMT_CODE_CLASS (TREE_CODE_CLASS (code)))
	{
	  int i, len;

	  /* Walk over all the sub-trees of this operand.  */
	  len = TREE_OPERAND_LENGTH (*tp);

	  /* Go through the subtrees.  We need to do this in forward order so
	     that the scope of a FOR_EXPR is handled properly.  */
	  if (len)
	    {
	      for (i = 0; i < len - 1; ++i)
		WALK_SUBTREE (GENERIC_TREE_OPERAND (*tp, i));
          tpp = tp;
	      WALK_SUBTREE_TAIL (GENERIC_TREE_OPERAND (*tp, len - 1));
	    }
	}

    }

  /* We didn't find what we were looking for.  */
  return NULL_TREE;

#undef WALK_SUBTREE_TAIL
}
#undef WALK_SUBTREE




/* Apply FUNC to all language-specific sub-trees of TP in a pre-order
   traversal.  Called from walk_tree.  */

static tree
cp_walk_subtrees_with_parent  (tree *tp,
                               int *walk_subtrees_p, walk_tree_fn_1 func)
{
  enum tree_code code = TREE_CODE (*tp);
  tree result;

#define WALK_SUBTREE(NODE)				\
  do							\
    {							\
      result = walk_tree_with_parent (&(NODE), tp, func);	\
      if (result) goto out;				\
    }							\
  while (0)

  /* Not one of the easy cases.  We must explicitly go through the
     children.  */
  result = NULL_TREE;
  switch (code)
    {
    case DEFAULT_ARG:
    case TEMPLATE_TEMPLATE_PARM:
    case BOUND_TEMPLATE_TEMPLATE_PARM:
    case UNBOUND_CLASS_TEMPLATE:
    case TEMPLATE_PARM_INDEX:
    case TEMPLATE_TYPE_PARM:
    case TYPENAME_TYPE:
    case ASSOCIATED_TYPE:
    case TYPEOF_TYPE:
      /* None of these have subtrees other than those already walked
	 above.  */
      *walk_subtrees_p = 0;
      break;

    case BASELINK:
      WALK_SUBTREE (BASELINK_FUNCTIONS (*tp));
      *walk_subtrees_p = 0;
      break;

    case TINST_LEVEL:
      WALK_SUBTREE (TINST_DECL (*tp));
      *walk_subtrees_p = 0;
      break;

    case PTRMEM_CST:
      WALK_SUBTREE (TREE_TYPE (*tp));
      *walk_subtrees_p = 0;
      break;

    case TREE_LIST:
      WALK_SUBTREE (TREE_PURPOSE (*tp));
      break;

    case OVERLOAD:
      WALK_SUBTREE (OVL_FUNCTION (*tp));
      WALK_SUBTREE (OVL_CHAIN (*tp));
      *walk_subtrees_p = 0;
      break;

    case RECORD_TYPE:
      if (TYPE_PTRMEMFUNC_P (*tp))
	WALK_SUBTREE (TYPE_PTRMEMFUNC_FN_TYPE (*tp));
      break;

    case DECLTYPE_TYPE:
       WALK_SUBTREE (DECLTYPE_TYPE_EXPR (*tp));
       *walk_subtrees_p = 0;
       break;
 
    case FUNCTION_DECL:
      break;

    case TYPE_ARGUMENT_PACK:
    case NONTYPE_ARGUMENT_PACK:
      {
        tree args = ARGUMENT_PACK_ARGS (*tp);
        int i, len = TREE_VEC_LENGTH (args);
        for (i = 0; i < len; i++)
          WALK_SUBTREE (TREE_VEC_ELT (args, i));
      }
      break;

    case TYPE_PACK_EXPANSION:
      WALK_SUBTREE (TREE_TYPE (*tp));
      *walk_subtrees_p = 0;
      break;
      
    case EXPR_PACK_EXPANSION:
      WALK_SUBTREE (TREE_OPERAND (*tp, 0));
      *walk_subtrees_p = 0;
      break;

    case CAST_EXPR:
      if (TREE_TYPE (*tp))
	WALK_SUBTREE (TREE_TYPE (*tp));

      {
        int i;
        for (i = 0; i < TREE_CODE_LENGTH (TREE_CODE (*tp)); ++i)
	  WALK_SUBTREE (TREE_OPERAND (*tp, i));
      }
      *walk_subtrees_p = 0;
      break;

    case TRAIT_EXPR:
      WALK_SUBTREE (TRAIT_EXPR_TYPE1 (*tp));
      WALK_SUBTREE (TRAIT_EXPR_TYPE2 (*tp));
      *walk_subtrees_p = 0;
      break;

    case REQUIREMENT:
      switch (WHERE_REQ_KIND (*tp))
	{
	case REQ_CONCEPT:
	case REQ_NOT_CONCEPT:
	  WALK_SUBTREE (WHERE_REQ_MODEL (*tp));
	  break;

	case REQ_SAME_TYPE:
	  WALK_SUBTREE (WHERE_REQ_FIRST_TYPE (*tp));
	  WALK_SUBTREE (WHERE_REQ_SECOND_TYPE (*tp));
	  break;

	case REQ_DERIVED_FROM:
	  WALK_SUBTREE (WHERE_REQ_DERIVED (*tp));
	  WALK_SUBTREE (WHERE_REQ_BASE (*tp));
	  break;

	case REQ_ICE:
	  WALK_SUBTREE (WHERE_REQ_CONSTANT_EXPRESSION (*tp));
	  break;
	}
      WALK_SUBTREE (WHERE_REQ_CHAIN (*tp));
      break;

    default:
      return NULL_TREE;
    }

  /* We didn't find what we were looking for.  */
 out:
  return result;

#undef WALK_SUBTREE
}

/* position operation */
static char* calc_cell_position (const char* ancestor, int index)
{
    char *buf;
    char tmp[5];
    int len;

    if (!ancestor)
    {
	buf = (char *) xmalloc (5);
	strcpy (buf, "1");
	return buf;
    }

    len = strlen (ancestor);
    buf = (char *) xmalloc (len + 5); 

    strcpy (buf, ancestor);
    strcat (buf, ".");
    sprintf (tmp, "%d", index);
    strcat (buf, tmp);
    
    
    return buf;
}

static char* calc_cell_position_for_sibling (const char* sibling)
{
    char *buf;
    char *result;
    char *token;
    char tmp[5];
    int len;

    if (!sibling) return 0;
    len = strlen (sibling);
    buf = (char *) xmalloc (len + 4);
    result = (char *) xmalloc (len + 4);
    strcpy (buf, sibling);
    strcat (buf, ".");
    strcpy (result, "\0");
    
    token = strtok (buf, ".");
    strcpy (tmp, token);
    while (token)
    {
	token = strtok (NULL, ".");
	if (token) 
	{
	    strcat (result, tmp);
	    strcat (result, ".");
	    strcpy (tmp, token);
	}
	else 
	    break;
    }
	
    /* now the last one */
    len = atoi (tmp) + 1;
    sprintf (tmp, "%d", len);
    strcat (result, tmp);
    
    return result;
}

static void assign_pos_to_cell (pattern_cell_ptr p, char* pos)
{
  pattern_cell_ptr brother;

  if (!p) return;
  /* assign the pos to the current cell */
  p->position = pos;

  /* child first */
  assign_pos_to_cell (p->next, calc_cell_position (pos, 1));

  /* brother */
  for (brother = p->sibling; brother; brother = brother->sibling)
    {
      pos = calc_cell_position_for_sibling (pos);
      assign_pos_to_cell (brother, pos);
    }
}

/* precedence table for the pattern cell */
/* pck_none pck_code pck_parm pck_var pck_const pck_fun pck_tree */
static const unsigned int pattern_cell_relation[8][8] = 
                      {
                      /*  none code parm var const ref fun  exp */
/* pck_none  */           {0,   0,   0,   0,   0,   0,   0,  0},
/* pck_code  */           {0,   1,   0,   0,   0,   0,   0,  0},
/* pck_parm  */           {0,   0,   1,   0,   0,   0,   0,  0},
/* pck_var   */           {0,   0,   1,   1,   1,   1,   1,  1},
/* pck_const */           {0,   0,   0,   0,   1,   0,   0,  0},
/* pck_ref   */           {0,   0,   0,   0,   0,   1,   0,  0},
/* pck_fun   */           {0,   0,   0,   0,   0,   0,   1,  0},
/* pck_exp   */           {0,   0,   0,   0,   0,   0,   0,  1}
                       };

/* return value:
 *  0 stands for complete match 
 *  1 stands for position mismatch
 *  2 stands for first one greater position 
 *  3 stands for type mismatch 
 *  4 stands for kind mismatch 
 *  5 stands for address or code mismatch */
static unsigned 
is_pattern_cell_match (pattern_cell_ptr first, pattern_cell_ptr second)
{
    gcc_assert (first);
    gcc_assert (second);

    /* match type */
    if ((first->type && !second->type)
        || (!first->type && second->type)) return 4;
    if (first->type && second->type
        && !same_type_p (first->type, second->type)) return 4;

    /* match kind */
    if (pattern_cell_relation[first->kind][second->kind] != 1) 
	return 5;

    if (first->kind == 4 && first->u.code != second->u.code) 
      return 7;

    /* match addresse */
    if (first->kind == 4 && first->u.code == REAL_CST 
        && !real_identical(TREE_REAL_CST_PTR (first->u.decl),
                           TREE_REAL_CST_PTR (second->u.decl)))
      return 6;

    if (first->kind == 4 && first->u.code != REAL_CST && 
        first->u.decl != second->u.decl)
      return 6;

    if (first->kind == 1 && first->u.code != second->u.code)
      return 7;

    if (first->kind == 3) 
      {
        VEC_safe_push (pattern_cell_ptr, gc, pattern_parms_stack, first);
        VEC_safe_push (pattern_cell_ptr, gc, subject_arguments_stack, second);
        return 1;
      }
    
    return 0;
}

static bool 
same_pattern_cell_p (pattern_cell_ptr first, pattern_cell_ptr second)
{
   pattern_cell_ptr axiom_cell_1;
   pattern_cell_ptr cand_cell_1;
   bool result = true;

  if (first->kind != second->kind) return false;
  if ((first->type && !second->type)
      || (!first->type && second->type)) return false;
  if (first->type && second->type
      && !same_type_p (first->type, second->type)) return false;

  if (first->kind == 0)
    ;
  else if (first->kind == 1) 
    {
    if (first->u.code != second->u.code) result = false;
    }
  else if (first->kind == 2)
    { 
   if (first->u.decl != second->u.decl) result = false; 
    }
  else if (first->kind == 3) 
    {  
      if (first->u.decl != second->u.decl) result = false; 
    }
  else if (first->kind == 4)
    { 
      if (first->u.code != second->u.code) 
        result = false;
      if (first->u.code == REAL_CST &&
          !real_identical(TREE_REAL_CST_PTR (first->u.decl),
                          TREE_REAL_CST_PTR (second->u.decl)))
        result = false;
      if (first->u.code != REAL_CST && first->u.decl != second->u.decl) 
        result = false;  
    }
  else if (first->kind > 4)
    {
    if (first->u.code != second->u.code) result = false; 
    }

  if (!result) return false;

  /* children */
      axiom_cell_1 = first->next;
      cand_cell_1 = second->next;
      for ( ; ; )
        {
          if (!axiom_cell_1 && !cand_cell_1) 
            {
              result = true;
              break;
            }
          /* stop comparing */
          if ((axiom_cell_1 && !cand_cell_1)
              || (!axiom_cell_1 && cand_cell_1)) 
            { 
              result = false;
              break;
            }
          result = same_pattern_cell_p (axiom_cell_1, cand_cell_1);
          if (!result) break;

          axiom_cell_1 = axiom_cell_1->sibling;
          cand_cell_1 = cand_cell_1->sibling;
        }
      return result;
}

static void transform_expr_with_concept_axiom (pattern_cell_ptr cell_header)
{
  axiom_expr_ptr axiom;

    gcc_assert (cell_header);
    
    if (!cell_header->next) return;

    for (axiom = axiom_expr_pattern_table;
         axiom; axiom = axiom->chain)
      {
        walk_cell_bfs (axiom, cell_header->next, "");
      }
}

static bool solve_equations (void) 
{
    pattern_cell_ptr var, value;
    pattern_cell_ptr var1, value1;
    unsigned ix, jx;
    tree prev, current;
    const char* first;
    const char* second;

    for (ix = 0; 
         VEC_iterate (pattern_cell_ptr, pattern_parms_stack, ix, var),
           VEC_iterate (pattern_cell_ptr, subject_arguments_stack, ix, value);
         ix++)
      {
        for (jx = ix + 1; 
             VEC_iterate (pattern_cell_ptr, pattern_parms_stack, jx, var1),
               VEC_iterate (pattern_cell_ptr, subject_arguments_stack, jx, value1);
             jx++)
          {
            if (same_pattern_cell_p(var, var1)
                && !same_pattern_cell_p(value, value1)) 
              return false;
          }
      }
    /* We intentionally compare the id in lexicographer order */
    prev = NULL_TREE;

    for (ix = 0; VEC_iterate (pattern_cell_ptr, subject_arguments_stack, ix, var);
           ix++)
      {
        current = var->u.decl;
        if (prev == NULL_TREE) 
          {
            prev = current;
          }
        else 
          {
            first = IDENTIFIER_POINTER (DECL_NAME (prev));
            second = IDENTIFIER_POINTER (DECL_NAME (current));
            /* compare them then */
            if (strcmp(first, second) > 0) return false;
          }
      }

    return true;
}

/* set operation */
static int index_of_pattern_parms (tree member)
{
    int ix;
    pattern_cell_ptr ptr;
    for (ix = 0; VEC_iterate (pattern_cell_ptr, pattern_parms_stack, ix, ptr); ix++) 
      if (*(ptr->orig_addr) == member) return ix;
    return -1;
}

static tree
replace_parm_with_arg (tree *tp, int *walk_subtrees, void *data ATTRIBUTE_UNUSED)
{
  enum tree_code code = TREE_CODE (*tp);
  int index;
  pattern_cell_ptr cell;

  if (code == VAR_DECL) 
    {
      index = index_of_pattern_parms (*tp);
      if (index != -1)
        { 
          cell = VEC_index (pattern_cell_ptr, subject_arguments_stack, index);
          *tp = *(cell->orig_addr);
          *walk_subtrees = 0;
          return NULL_TREE;
        }
    }
  return NULL_TREE;
}

static void 
walk_cell_bfs (axiom_expr_ptr axiom_cell,
               pattern_cell_ptr cand_cell, const char* prefix)
{
  bool result = false;
  pattern_cell_ptr p; 

  if (!pattern_parms_stack) 
	pattern_parms_stack = VEC_alloc (pattern_cell_ptr, gc, 10);
  else
	VEC_truncate (pattern_cell_ptr, pattern_parms_stack, 0);
    
  if (!subject_arguments_stack)
	subject_arguments_stack = VEC_alloc (pattern_cell_ptr, gc, 10);
  else
	VEC_truncate (pattern_cell_ptr, subject_arguments_stack, 0);
 
  result = match_and_transform (axiom_cell->cell_p->next, cand_cell, prefix);
  if (result) 
    {
      bool isOk;
      fprintf (stdout, "The front-end concept-based optimizer would check a match\n");
      /* transform */
      /* step 1: equations solution */
      isOk = solve_equations ();
      /* step 2: transformer */
      if (isOk)
        {
          tree t = axiom_cell->template;
          walk_tree (&t, &copy_tree_r, NULL, NULL);
          walk_tree (&t, &replace_parm_with_arg, NULL, NULL);
          *(cand_cell->orig_addr) = t;
          return;
        }
    }

  for (p = cand_cell->next; p; p = p->sibling)
    {
      walk_cell_bfs (axiom_cell, p, cand_cell->position);
    }

}


static bool 
match_and_transform (pattern_cell_ptr axiom_cell,
                     pattern_cell_ptr cand_cell, const char* prefix)
{
    pattern_cell_ptr axiom_cell_1;
    pattern_cell_ptr cand_cell_1;
    unsigned result;
    bool r;

    /* commpare the current cell */
	result = is_pattern_cell_match (axiom_cell, cand_cell);    
	switch (result) 
	{
    case 0: /* match */
    /* all of its children */
      axiom_cell_1 = axiom_cell->next;
      cand_cell_1 = cand_cell->next;
      for ( ; ; )
        {
          if (!axiom_cell_1 && !cand_cell_1) 
              return true;

          /* stop comparing */
          if ((axiom_cell_1 && !cand_cell_1)
              || (!axiom_cell_1 && cand_cell_1)) 
            return false;

          r = match_and_transform (axiom_cell_1, cand_cell_1, prefix);
          if (!r) return false;
          axiom_cell_1 = axiom_cell_1->sibling;
          cand_cell_1 = cand_cell_1->sibling;
        }
      break;
    case 1: /* ref match */
      break;
    default: /* non match */
      return false;
	}
    return true;
}

/* debug facility */
static void print_pattern_table (void)
{
    int a, b;
    axiom_expr_ptr p;

    if (!axiom_expr_pattern_table) 
    {
	fprintf (stdout, "%s", "\nnull table\n");
	return;
    }

    fprintf (stdout, "\n----------------begin----------------------");
    fprintf (stdout, "\nThe axiom pattern table is as follows:\n");
 
    for (p = axiom_expr_pattern_table; p; p = p->chain)
      {
    a = 1; 
    b = 1;
    sum_row_and_col (p->cell_p->next, &a, &b, a, b);
      }

    fprintf (stdout, "-------------------end----------------------\n");
}

/* debug facility */
static void print_pattern_expr (pattern_cell_ptr p)
{
    int a, b;
    if (!p) 
    {
	fprintf (stdout, "%s", "\nnull expression\n");
	return;
    }

    fprintf (stdout, "\n----------------begin----------------------");
    fprintf (stdout, "\nThe subject term is as follows:\n");
    a = 1; 
    b = 1;
    sum_row_and_col (p->next, &a, &b, a, b);
    fprintf (stdout, "-------------------end----------------------\n");
}


static void print_pattern_cell (pattern_cell_ptr cell)
{
    if (!cell) return;

    fprintf (stdout, "\t[");
    fprintf (stdout, "postion: %s", cell->position);

    fprintf (stdout, "\t");		
    fprintf (stdout, "type:\t%x\t", (unsigned) cell->type);
    
    if (cell->kind == pck_none) 
	fprintf (stdout, "pck_none_cell");
    else if (cell->kind == pck_code)
	fprintf (stdout, "pck_code_cell");
    else if (cell->kind == pck_parm)
	fprintf (stdout, "pck_parm_cell");
    else if (cell->kind == pck_var)
	fprintf (stdout, "pck_var_cell");
    else if (cell->kind == pck_const)
	fprintf (stdout, "pck_const_cell");
    else if (cell->kind == pck_ref)
	fprintf (stdout, "pck_ref_cell");
    else if (cell->kind == pck_fun)
	fprintf (stdout, "pck_fun_cell");
    else if (cell->kind == pck_exp)
	fprintf (stdout, "pck_exp_cell");
    else
	fprintf (stdout, "unknown_cell");

    if (cell->kind > 0 && cell->kind < 8)
      { 
        fprintf (stdout, "\t%s", tree_code_name [(unsigned) cell->u.code]);
        fprintf (stdout, "\t%x", (unsigned) cell->u.decl);     
      }
    fprintf (stdout, "]\n");

}

static void 
sum_row_and_col (pattern_cell_ptr head, 
	int *row, 
	int *col, 
	int startx, 
	int starty) 
{
    int z = starty;
    pattern_cell_ptr tmp_cell;
    if (!head)
    {
	fprintf (stdout, "\n");
	*row = *row + 1;
	return;
    }
    /* print out the coordinate for the cell and the value */
    startx = *row;
    fprintf (stdout, "(%d,%d)", *row, starty);
    print_pattern_cell (head);

    sum_row_and_col (head->next, row, col, startx, starty + 1);

    tmp_cell = head->sibling;
    while (tmp_cell)
    {
	fprintf (stdout, "(%d, %d)", *row, z);
	print_pattern_cell (tmp_cell);
	sum_row_and_col (tmp_cell->next, row, col, startx, z + 1);
	tmp_cell = tmp_cell->sibling;
    }
}


