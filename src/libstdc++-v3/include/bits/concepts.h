// Concept definitions -*- C++ -*- .

// Copyright (C) 2005, 2006 The Trustees of Indiana University
// Authors: Douglas Gregor, Jeremiah Willcock, and Andrew Lumsdaine
//

// This file is part of the GNU ISO C++ Library.  This library is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2, or (at your option)
// any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along
// with this library; see the file COPYING.  If not, write to the Free
// Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.

// As a special exception, you may use this file as part of a free software
// library without restriction.  Specifically, if other files instantiate
// templates or use macros or inline functions from this file, or you compile
// this file and link it with other files to produce an executable, this
// file does not by itself cause the resulting executable to be covered by
// the GNU General Public License.  This exception does not however
// invalidate any other reasons why the executable file might be covered by
// the GNU General Public License.

/* IURTC PROVIDES THE INTELLECTUAL PROPERTY "AS IS" AND MAKES NO
REPRESENTATIONS AND EXTENDS NO WARRANTIES OF ANY KIND, EITHER EXPRESS
OR IMPLIED. THERE ARE NO EXPRESS OR IMPLIED WARRANTIES OF
MERCHANTABILITY OR FITNESS OF THE INTELLECTUAL PROPERTY OR LICENSED
PRODUCTS DERIVED FROM OR INCLUDING IT FOR A PARTICULAR PURPOSE, OR
THAT THE USE OF THE INTELLECTUAL PROPERTY OR ANY LICENSED PRODUCT WILL
NOT INFRINGE ANY PATENT, COPYRIGHT, TRADEMARK OR OTHER RIGHTS, OR ANY
OTHER EXPRESS OR IMPLIED WARRANTIES. IURTC MAKES NO REPRESENTATION OR
WARRANTY WITH RESPECT TO THE PERFORMANCE OF THE INTELLECTUAL PROPERTY
OR ANY LICENSED PRODUCT, INCLUDING THEIR SAFETY, EFFECTIVENESS, OR
COMMERCIAL VIABILITY. IURTC WILL NOT BE LIABLE TO [LICENSEE], OR ITS
SUCCESSORS, ASSIGNS, CONTRACTORS, OR SUBLICENSEES, OR ANY THIRD PARTY
REGARDING ANY CLAIM ARISING FROM OR RELATING TO [LICENSEE]'S USE OF
THE INTELLECTUAL PROPERTY, ANY LICENSED PRODUCT, OR FROM THE
MANUFACTURE, USE, IMPORTATION OR SALE OF LICENSED PRODUCTS, OR FOR ANY
CLAIM FOR LOSS OF PROFITS, LOSS OR INTERRUPTION OF BUSINESS, OR FOR
INDIRECT, SPECIAL, EXEMPLARY, PUNITIVE, OR CONSEQUENTIAL DAMAGES OF
ANY KIND. */

/** @file concepts.h
 *  This is an internal header file, included by other library headers.
 *  You should not attempt to use it directly.
 */
#ifndef _CONCEPTS_H
#define _CONCEPTS_H

#pragma GCC system_header
// So variadic macros won't generate a warning

#include <cstddef>
#include <bits/c++config.h>

// We can't have concepts if they're turned off in the compiler
#if !defined(__GXX_CONCEPTS__) && !defined(_GLIBCXX_NO_CONCEPTS)
#  define _GLIBCXX_NO_CONCEPTS
#endif

#ifdef _GLIBCXX_NO_CONCEPTS

#define _GLIBCXX_WHERE(...)
#define _GLIBCXX_LATE_CHECK
#define _GLIBCXX_REQ_PARM(Req,Name) typename Name
#define _GLIBCXX_PARM_REQ(Name, ...) typename Name
#define _GLIBCXX_ITERATOR_TRAITS(Concept,Name) \
  typename ::std::iterator_traits< Name >
#define _GLIBCXX_ITERATOR_TRAITS_NA(Name,Type) \
  typename ::std::iterator_traits< Name >::Type
#define _GLIBCXX_ITERATOR_TRAITS_NEST(Name,Type) \
  typename ::std::iterator_traits< Name >::Type
#define _GLIBCXX_CONCEPT_ALGO(Name) _GLIBCXX_CONCEPT_ALGO2( __ , Name )
#define _GLIBCXX_CONCEPT_ALGO2(X,Y) _GLIBCXX_CONCEPT_ALGO3(X,Y)
#define _GLIBCXX_CONCEPT_ALGO3(X,Y) X##Y

#else

#define _GLIBCXX_WHERE(...) requires __VA_ARGS__
#define _GLIBCXX_LATE_CHECK late_check
#define _GLIBCXX_REQ_PARM(Req,Name) Req Name
#define _GLIBCXX_PARM_REQ(Name, ...) __VA_ARGS__ Name
#define _GLIBCXX_ITERATOR_TRAITS(Concept,Name) \
  ::std::Concept< Name >
#define _GLIBCXX_ITERATOR_TRAITS_NA(Name,Type) Type
#define _GLIBCXX_ITERATOR_TRAITS_NEST(Name,Type) Name::Type
#define _GLIBCXX_CONCEPT_ALGO(Name) Name

namespace std
{

  // [concept.support]

  /// @brief Describes types that can be used as the return type of a
  /// function.
  concept Returnable<typename T> { }

  /// @brief Describes types to which a pointer can be formed.
  concept PointeeType<typename T> { }

  /// @brief Describes types to which a reference can be formed.
  concept ReferentType<typename T> : PointeeType<T> { }

  /// @brief Describes types that can be used to declare a variable.
  concept VariableType<typename T> { }

  /// @brief Describes object types.
  concept ObjectType<typename T> : VariableType<T> { }

  /// @brief Describes class types
  concept ClassType<typename T> : ObjectType<T> { }

  /// @brief Describes classes (and structs)
  concept Class<typename T> : ClassType<T> { }

  /// @brief Describes unions
  concept Union<typename T> : ClassType<T> { }

  /// @brief Describes trivial types
  concept TrivialType<typename T> : ObjectType<T> { }

  /// @brief Describes standard layout types
  concept StandardLayoutType<typename T> : ObjectType<T> { }

  /// @brief Describes literal types. 
  concept LiteralType<typename T> : ObjectType<T> { }

  /// @brief Describes scalar types
  concept ScalarType<typename T> 
    : TrivialType<T>, LiteralType<T>, StandardLayoutType<T> { }

  /// @brief Describes types that can be used as the type of a
  /// non-type template parameter.
  concept NonTypeTemplateParameterType<typename T> : VariableType<T> { }

  /// @brief Describes the types that can be the types of an integral
  /// constant expression.
  concept IntegralConstantExpressionType<typename T> 
    : ScalarType<T>, NonTypeTemplateParameterType<T> { }

  /// @brief Describes integral types
  concept IntegralType<typename T> : IntegralConstantExpressionType<T> { }

  /// @brief Describes enumeration types
  concept EnumerationType<typename T> : IntegralConstantExpressionType<T> { }

  /// @brief The SameType concept is a compiler-supported concept that
  /// requires that the two types, T and U, be equivalent.
  concept SameType<typename T, typename U> { }

  /**
   *  @if maint
   *  This concept map is used to match the SameType constraint when
   *  needed.
   *  @endif
  */
  template<typename T> concept_map SameType<T, T> { }

  /// @brief The DerivedFrom concept is a compiler-supported concept
  /// that requires that Derived by publicly and unambiguously derived
  /// from Base.
  concept DerivedFrom<typename Derived, typename Base> { }

  // [concept.comparison]

  /// @brief Describes an ordering between two objects (potentially of
  /// different types).
  auto concept LessThanComparable<typename T, typename U = T>
  {
    bool operator<(const T&, const U&);
    bool operator>(U a, T b) { return b < a; }
    bool operator<=(U a, T b) { return !(b < a); }
    bool operator>=(T a, U b) { return !(a < b); }
  }

  /// @brief Describes comparison of two objects for equality, which
  /// may have different types.
  auto concept EqualityComparable<typename T, typename U = T>
  {
    bool operator==(const T& t, const U& u);
    bool operator!=(const T& t, const U& u) { return !(t == u); }
  }
  
  // [concept.destruct]

  /// @brief Describes types that can be destroyed
  auto concept Destructible<typename T> : VariableType<T> { 
    T::~T();
  }
  
  // [concept.construct]

  // TODO: Can't express the Constructible concept just yet, because
  // we don't have support for variadic concepts.

  /// @brief Describes types that can be default-constructed
  auto concept DefaultConstructible<typename T> : Destructible<T>
  {
    T::T();
  }
  

  // [concept.copymove]
  
  /// @brief Describes types that can be move-constructed
  auto concept MoveConstructible<typename T> : Destructible<T>
  {
    T::T(T&&);
  }

  /// @brief Describes types that can be copy-constructed

  /// TODO: Should refine MoveConstructible, but there's a problem
  /// with the move-constructibility of references.
  auto concept CopyConstructible<typename T> 
  {
    T::T(const T&);
  }

  /// @brief Describes types that are trivially copy constructible
  concept TriviallyCopyConstructible<typename T> : CopyConstructible<T> { }

  /// @brief Describes types that are move-assignable
  auto concept MoveAssignable<typename T, typename U = T> {
    typename result_type;
    result_type T::operator=(U&&);
  }

  /// @brief Describes types that are copy-assignable
  auto concept CopyAssignable<typename T, typename U = T> : MoveAssignable<T, U>
  {
    typename result_type;
    result_type T::operator=(const U&);
  }

  /// @brief Describes types that are trivially copy-assignable,
  /// meaning that they can be copied with @c memcpy.
  concept TriviallyCopyAssignable<typename T> : CopyAssignable<T> { }

  /// @brief Describes types that are swappable
  auto concept Swappable<typename T>
  {
    void swap(T&, T&);
  }
  
  // [concept.memory]

  /// TODO: Need variadic concepts to do this properly
  auto concept Newable<typename T> : ObjectType<T> {
    void* T::operator new(size_t size);
  }

  /// @brief Describes types that can be deleted
  auto concept Deletable<typename T> : ObjectType<T> {
    void T::operator delete(void*);
  }

  /// TODO: Need variadic concepts to do this properly
  auto concept ArrayNewable<typename T> : ObjectType<T> {
    void* T::operator new[](size_t size);
  }

  /// @brief Describes types that can be deleted
  auto concept ArrayDeletable<typename T> : ObjectType<T> {
    void T::operator delete[](void*);
  }

  /// @brief Describes types that can be allocated onto and freed from
  /// the heap.
  auto concept HeapAllocatable<typename T>
    : Newable<T>, Deletable<T>, ArrayNewable<T>, ArrayDeletable<T> { }
  

  // [concept.regular]

  /// @brief Describes a semi-regular type, which can be copied and
  /// assigned.
  /// TODO: the CopyAssignable requirement should be a refinement?
  auto concept Semiregular<typename T> : CopyConstructible<T>
  { 
    requires CopyAssignable<T> && SameType<CopyAssignable<T>::result_type, T&>;
  }
  
  /// @brief Describes a regular type.
  auto concept Regular<typename T>
  : Semiregular<T>, DefaultConstructible<T>, EqualityComparable<T>,
    HeapAllocatable<T>
  {
  }

  // [concept.convertible]

  // TODO: Cannot write the ExplicitlyConvertible concept without
  // support for explicit conversion operators

  /// @brief Describes the ability to (implicitly) convert from one
  /// type to another.
  auto concept Convertible<typename T, typename U>
  {
    operator U(const T&);
  }
  
  /// TODO: Helper concept map to be removed
  template<typename T>
  concept_map Convertible<T, T> {}
  
  /// TODO: Helper concept map to be removed
  template<typename T>
  concept_map Convertible<T, T&> { }
  
  /// TODO: Helper concept map to be removed
  template<typename T>
  concept_map Convertible<T, const T&> { }


  // [concept.true]

  // @btief The True concept is a compiler-supported concept that
  // requires that the integral constant expression it is given
  // evaluate true.
  concept True<bool> { }

  /**
   * @if maint
   * Helper concept map implementing the semantics of concept True
   * @endif
  */
  concept_map True<true> { }

  // [concept.operator]

  /// @brief Describes types with a binary @c operator+
  auto concept HasPlus<typename T, typename U = T>
  {
    typename result_type;
    result_type operator+(T const& t, U const& u);
    result_type operator+(T const& t, U&& u);
    result_type operator+(T&& t, U const& u);
    result_type operator+(T&& t, U&& u);
  }

  /// @brief Describes types with a binary @c operator-
  auto concept HasMinus<typename T, typename U = T>
  {
    typename result_type;
    result_type operator-(T const& t, U const& u);
    result_type operator-(T const& t, U&& u);
    result_type operator-(T&& t, U const& u);
    result_type operator-(T&& t, U&& u);
  }

  /// @brief Describes types with a binary @c operator*
  auto concept HasMultiply<typename T, typename U = T>
  {
    typename result_type;
    result_type operator*(T const& t, U const& u);
    result_type operator*(T const& t, U&& u);
    result_type operator*(T&& t, U const& u);
    result_type operator*(T&& t, U&& u);
  }

  /// @brief Describes types with a binary @c operator/
  auto concept HasDivide<typename T, typename U = T>
  {
    typename result_type;
    result_type operator/(T const& t, U const& u);
    result_type operator/(T const& t, U&& u);
    result_type operator/(T&& t, U const& u);
    result_type operator/(T&& t, U&& u);
  }

  /// @brief Describes types with a binary @c operator%
  auto concept HasModulus<typename T, typename U = T>
  {
    typename result_type;
    result_type operator%(T const& t, U const& u);
    result_type operator%(T const& t, U&& u);
    result_type operator%(T&& t, U const& u);
    result_type operator%(T&& t, U&& u);
  }

  /// @brief Describes types with a unary @c operator+
  auto concept HasUnaryPlus<typename T>
  {
    typename result_type;
    result_type operator+(T const& t);
    result_type operator+(T&& t);
  }

  /// @brief Describes types with a unary @c operator-
  auto concept HasNegate<typename T>
  {
    typename result_type;
    result_type operator-(T const& t);
    result_type operator-(T&& t);
  }
  
  /// @brief Describes types with a binary @c operator&&
  auto concept HasLogicalAnd<typename T, typename U = T>
  {
    bool operator&&(T const& t, U const& u);
    bool operator&&(T const& t, U&& u);
    bool operator&&(T&& t, U const& u);
    bool operator&&(T&& t, U&& u);
  }

  /// @brief Describes types with a binary @c operator||
  auto concept HasLogicalOr<typename T, typename U = T>
  {
    bool operator||(T const& t, U const& u);
    bool operator||(T const& t, U&& u);
    bool operator||(T&& t, U const& u);
    bool operator||(T&& t, U&& u);
  }

  /// @brief Describes types with a unary @c operator!
  auto concept HasLogicalNot<typename T>
  {
    bool operator!(T const& t);
    bool operator!(T&& t);
  }

  /// @brief Describes types with a binary @c operator&
  auto concept HasBitAnd<typename T, typename U = T>
  {
    typename result_type;
    result_type operator&(T const& t, U const& u);
    result_type operator&(T const& t, U&& u);
    result_type operator&(T&& t, U const& u);
    result_type operator&(T&& t, U&& u);
  }

  /// @brief Describes types with a binary @c operator|
  auto concept HasBitOr<typename T, typename U = T>
  {
    typename result_type;
    result_type operator|(T const& t, U const& u);
    result_type operator|(T const& t, U&& u);
    result_type operator|(T&& t, U const& u);
    result_type operator|(T&& t, U&& u);
  }

  /// @brief Describes types with a binary @c operator^
  auto concept HasBitXor<typename T, typename U = T>
  {
    typename result_type;
    result_type operator^(T const& t, U const& u);
    result_type operator^(T const& t, U&& u);
    result_type operator^(T&& t, U const& u);
    result_type operator^(T&& t, U&& u);
  }
  
  /// @brief Describes types with a unary @c operator~
  auto concept HasComplement<typename T>
  {
    typename result_type;
    result_type operator~(T const& t);
    result_type operator~(T&& t);
  }

  /// @brief Describes types with a binary @c operator<<
  auto concept HasLeftShift<typename T, typename U = T>
  {
    typename result_type;
    result_type operator<<(T const& t, U const& u);
    result_type operator<<(T const& t, U&& u);
    result_type operator<<(T&& t, U const& u);
    result_type operator<<(T&& t, U&& u);
  }

  /// @brief Describes types with a binary @c operator<<
  auto concept HasRightShift<typename T, typename U = T>
  {
    typename result_type;
    result_type operator>>(T const& t, U const& u);
    result_type operator>>(T const& t, U&& u);
    result_type operator>>(T&& t, U const& u);
    result_type operator>>(T&& t, U&& u);
  }
  
  /// @brief Describes types that have a unary @c operator*
  auto concept Dereferenceable<typename T>
  {
    typename reference;
    reference operator*(T);
  };


  /// @brief Describes types that have an address-of operator
  auto concept Addressable<typename T> 
  {
    typename pointer;
    typename const_pointer;

    pointer operator&(T&);
    const_pointer operator&(T const&);
  }

  // TODO: cannot express Callable, which is variadic, so we settle
  // for Callable0, Callable1, etc.

  auto concept Callable0<typename F> {
    typename result_type;
    result_type operator()(F&);
  };
  
  auto concept Callable1<typename F, typename X> {
    typename result_type;
    result_type operator()(F&, X);
  };
  
  auto concept Callable2<typename F, typename X, typename Y> {
    typename result_type;
    result_type operator()(F&, X, Y);
  };
  
  // [concept.arithmetic]

  /// @brief Describes types that provide all of the operators
  /// available on arithmetic types.
  concept ArithmeticLike<typename T> : Regular<T>, LessThanComparable<T>
  {
    // TODO: These should be refinements...
    requires HasPlus<T> && HasMinus<T> && HasMultiply<T> && HasDivide<T>
          && HasUnaryPlus<T> && HasNegate<T>;

    T::T(long long);

    T& operator++(T&);
    T operator++(T& t, int) { T tmp(t); ++t; return tmp; }
    T& operator--(T&);
    T operator--(T& t, int) { T tmp(t); --t; return tmp; }

    requires Convertible<HasUnaryPlus<T>::result_type, T>
          && Convertible<HasNegate<T>::result_type, T>
          && Convertible<HasPlus<T>::result_type, T>
          && Convertible<HasMinus<T>::result_type, T>
          && Convertible<HasMultiply<T>::result_type, T>
          && Convertible<HasDivide<T>::result_type, T>;
    
    T& operator*=(T&, T);
    T& operator/=(T&, T);
    T& operator+=(T&, T);
    T& operator-=(T&, T);
  }
  

  /// @brief Describes types that act like integral types
  concept IntegralLike<typename T> : ArithmeticLike<T>
  {
    // TODO: these should be refinements
    requires HasComplement<T> && HasModulus<T> && HasBitAnd<T>
          && HasBitOr<T> && HasBitXor<T> && HasLeftShift<T> 
          && HasRightShift<T>;

    requires Convertible<HasComplement<T>::result_type, T>
          && Convertible<HasModulus<T>::result_type, T>
          && Convertible<HasBitAnd<T>::result_type, T>
          && Convertible<HasBitOr<T>::result_type, T>
          && Convertible<HasBitXor<T>::result_type, T>
          && Convertible<HasLeftShift<T>::result_type, T>
          && Convertible<HasRightShift<T>::result_type, T>;

    T& operator%=(T&, T);
    T& operator&=(T&, T);
    T& operator|=(T&, T);
    T& operator^=(T&, T);
    T& operator<<=(T&, T);
    T& operator>>=(T&, T);
  };

  /// @brief Describes types that act like signed integral types
  concept SignedIntegralLike<typename T> : IntegralLike<T>
  {
  };
  
  /// @brief Describes types that act like unsigned integral types
  concept UnsignedIntegralLike<typename T> : IntegralLike<T>
  {
  };
  
  concept_map IntegralLike<char> {};
  concept_map SignedIntegralLike<signed char> {};
  concept_map UnsignedIntegralLike<unsigned char> {};
#ifdef _GLIBCXX_USE_WCHAR_T
  concept_map IntegralLike<wchar_t> {};
#endif
  concept_map SignedIntegralLike<short> {};
  concept_map UnsignedIntegralLike<unsigned short> {};
  concept_map SignedIntegralLike<int> {};
  concept_map UnsignedIntegralLike<unsigned int> {};
  concept_map SignedIntegralLike<long> {};
  concept_map UnsignedIntegralLike<unsigned long> {};
  concept_map SignedIntegralLike<long long> {};
  concept_map UnsignedIntegralLike<unsigned long long> {};

  /// @brief Describes floating-point types
  concept FloatingPointLike<typename T> : ArithmeticLike<T> { }


  concept_map FloatingPointLike<float> { }
  concept_map FloatingPointLike<double> { }

  // [concept.predicate]

  // TODO: since we can't express the variadic Predicate, we just use
  // Predicate and BinaryPredicate for now

auto concept Predicate<typename F, typename X> : Callable1<F, X>
{
  requires Convertible<result_type, bool>;
};

auto concept BinaryPredicate<typename F, typename X, typename Y = X>
: Callable2<F, X, Y>
{
  requires Convertible<result_type, bool>;
};

  // Core concepts not defined in the Standard
  auto concept ConvertibleToIntegral<typename T>
  {
    operator long long(const T&);
  };

}
#endif // ndef _GLIBCXX_NO_CONCEPTS

#endif // _CONCEPTS_H
