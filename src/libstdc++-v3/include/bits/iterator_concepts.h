// Concept definitions -*- C++ -*- .

// Copyright (C) 2005, 2006, 2007 The Trustees of Indiana University
// Authors: Douglas Gregor, Jeremiah Willcock, and Andrew Lumsdaine
//

// This file is part of the GNU ISO C++ Library.  This library is free
// software; you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2, or (at your option)
// any later version.

// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along
// with this library; see the file COPYING.  If not, write to the Free
// Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.

// As a special exception, you may use this file as part of a free software
// library without restriction.  Specifically, if other files instantiate
// templates or use macros or inline functions from this file, or you compile
// this file and link it with other files to produce an executable, this
// file does not by itself cause the resulting executable to be covered by
// the GNU General Public License.  This exception does not however
// invalidate any other reasons why the executable file might be covered by
// the GNU General Public License.

/* IURTC PROVIDES THE INTELLECTUAL PROPERTY "AS IS" AND MAKES NO
REPRESENTATIONS AND EXTENDS NO WARRANTIES OF ANY KIND, EITHER EXPRESS
OR IMPLIED. THERE ARE NO EXPRESS OR IMPLIED WARRANTIES OF
MERCHANTABILITY OR FITNESS OF THE INTELLECTUAL PROPERTY OR LICENSED
PRODUCTS DERIVED FROM OR INCLUDING IT FOR A PARTICULAR PURPOSE, OR
THAT THE USE OF THE INTELLECTUAL PROPERTY OR ANY LICENSED PRODUCT WILL
NOT INFRINGE ANY PATENT, COPYRIGHT, TRADEMARK OR OTHER RIGHTS, OR ANY
OTHER EXPRESS OR IMPLIED WARRANTIES. IURTC MAKES NO REPRESENTATION OR
WARRANTY WITH RESPECT TO THE PERFORMANCE OF THE INTELLECTUAL PROPERTY
OR ANY LICENSED PRODUCT, INCLUDING THEIR SAFETY, EFFECTIVENESS, OR
COMMERCIAL VIABILITY. IURTC WILL NOT BE LIABLE TO [LICENSEE], OR ITS
SUCCESSORS, ASSIGNS, CONTRACTORS, OR SUBLICENSEES, OR ANY THIRD PARTY
REGARDING ANY CLAIM ARISING FROM OR RELATING TO [LICENSEE]'S USE OF
THE INTELLECTUAL PROPERTY, ANY LICENSED PRODUCT, OR FROM THE
MANUFACTURE, USE, IMPORTATION OR SALE OF LICENSED PRODUCTS, OR FOR ANY
CLAIM FOR LOSS OF PROFITS, LOSS OR INTERRUPTION OF BUSINESS, OR FOR
INDIRECT, SPECIAL, EXEMPLARY, PUNITIVE, OR CONSEQUENTIAL DAMAGES OF
ANY KIND. */

/** @file iterator_concepts.h
 *  This is an internal header file, included by other library headers.
 *  You should not attempt to use it directly.
 */
#ifndef _ITERATOR_CONCEPTS_H
#define _ITERATOR_CONCEPTS_H

#pragma GCC system_header

#include <bits/concepts.h>

#ifndef _GLIBCXX_NO_CONCEPTS
namespace std 
{
  concept IteratorBase<typename X>
  {
    typename value_type = X::value_type;
    CopyConstructible reference = typename X::reference;
    typename pointer = X::pointer;
  };

  // 24.1.1 (Table 73)
  concept InputIterator<typename X>
  : IteratorBase<X>, Semiregular<X>,
    EqualityComparable<X>
  {
    SignedIntegralLike difference_type = X::difference_type;

    requires CopyAssignable<X>, SameType<CopyAssignable<X>::result_type, X&>;
    requires Convertible<reference, value_type>;
    requires Convertible<pointer, const value_type*>;

    Dereferenceable postincrement_result = X;
    requires Convertible<Dereferenceable<postincrement_result>::reference,
                        value_type>;

    pointer operator->(X);
    X& operator++(X&);
    postincrement_result operator++(X&, int);
    reference operator*(X);
  };

  // 24.1.2 (Table 74)
  // Note: We split OutputIterator into two concepts: OutputIterator,
  // which requires that one specify the value type, and
  // BasicOutputIterator, which has a fixed value type and serves as the
  // superior concept to MutableForwardIterator. A model template maps
  // from BasicOutputIterator to OutputIterator when needed.
  concept OutputIterator<typename X, typename Value>
  {
    requires CopyConstructible<X>;

    CopyAssignable<Value> reference = typename X::reference;
    typename postincrement_result = X;
    requires Dereferenceable<postincrement_result&> &&
            Convertible<postincrement_result, const X&> &&
            CopyAssignable<Dereferenceable<postincrement_result&>::reference,
                           Value>;


    reference operator*(X&);
    X& operator++(X&);
    postincrement_result operator++(X&, int);
  };

  concept BasicOutputIterator<typename X>
    : IteratorBase<X>
  {
    requires CopyConstructible<X>;
    requires CopyAssignable<reference, value_type>;

    typename postincrement_result = X;
    requires Dereferenceable<postincrement_result&>,
            CopyAssignable<Dereferenceable<postincrement_result&>::reference,
                       value_type>,
            Convertible<postincrement_result, const X&>;

    reference operator*(X&);
    X& operator++(X&);
    postincrement_result operator++(X&, int);
  };

  // 24.1.3 (Table 75)

  // Note: This table actually specifies both a mutable and a
  // non-mutable forward iterator concepts. We have to split these into
  // two concepts. The mutable concept refines the non-mutable version.
  concept ForwardIterator<typename X>
    : InputIterator<X>, Regular<X>
  {
    requires Convertible<postincrement_result, const X&>;
  };

  concept MutableForwardIterator<typename X>
    : ForwardIterator<X>,
      BasicOutputIterator<X>
  {
    requires CopyConstructible<value_type>;
  };

  _GLIBCXX_LATE_CHECK
  template<BasicOutputIterator X, typename Value>
  requires CopyAssignable<BasicOutputIterator<X>::reference, Value>
  concept_map OutputIterator<X, Value>
  {
    typedef BasicOutputIterator<X>::reference       reference;
    typedef BasicOutputIterator<X>::postincrement_result postincrement_result;
  };

  // 24.1.4 (Table 76)

  // Note: This table actually specifies both a mutable and a
  // non-mutable bidirectional iterator concepts. We have to split these
  // into two concepts. The mutable concept refines the non-mutable
  // version.
  concept BidirectionalIterator<typename X>
    : ForwardIterator<X>
  {
    Dereferenceable postdecrement_result = X;
    requires Convertible<Dereferenceable<postdecrement_result>::reference, 
                        value_type>,
            Convertible<postdecrement_result, const X&>;

    X& operator--(X&);
    postdecrement_result operator--(X&, int);
  };

  concept MutableBidirectionalIterator<typename X>
    : BidirectionalIterator<X>,
      MutableForwardIterator<X>
  {
  };

  // 24.1.5 (Table 77)

  // Note: This table actually specifies both a mutable and a
  // non-mutable bidirectional iterator concepts. We have to split these
  // into two concepts. The mutable concept refines the non-mutable
  // version.
  concept RandomAccessIterator<typename X>
   : BidirectionalIterator<X>,
     LessThanComparable<X>
  {
    X& operator+=(X&, difference_type);
    X  operator+ (X, difference_type);
    X  operator+ (difference_type, X);
    X& operator-=(X&, difference_type);
    X  operator- (X, difference_type);
    difference_type operator-(X, X);
    reference operator[](X, difference_type);
  };

  concept MutableRandomAccessIterator<typename X>
    : RandomAccessIterator<X>,
      MutableBidirectionalIterator<X>
  {
  };

  // 23.1 (Table 66)
  concept Container<typename X>
    : DefaultConstructible<X>,
      CopyConstructible<X>,
      EqualityComparable<X>,
      LessThanComparable<X>
  {
    requires CopyAssignable<X>, SameType<CopyAssignable<X>::result_type, X&>;

    typename value_type = X::value_type;

    // Note: The standard says "lvalue of T", but the term isn't
    // defined. We assume that means that it needs to be convertible to
    // a non-const reference to the value_type.
    typename reference = X::reference;
    requires Convertible<reference, value_type&>;

    // Note: The standard says "const lvalue of T", but the term isn't
    // defined. We assume that means that it needs to be convertible to
    // a const reference to the value_type.
    typename const_reference = X::const_reference;
    requires Convertible<const_reference, const value_type&>;

    // Note: The standard says "any category except output iterator."
    // This does not mean that the iterator cannot model the
    // OutputIterator concept; it means that we're requiring an
    // InputIterator.
    // Note: The standard says "iterator type" instead of "const
    // iterator type", implying that there may be some mutability
    // here. However, it also states that the output iterator category
    // is not allowed. We interpret this to mean that the true
    // requirement is just InputIterator, but one could equally well
    // argue for both InputIterator and OutputIterator.
    ForwardIterator iterator = typename X::iterator;
    requires SameType<InputIterator<iterator>::value_type, value_type>;

    // Note: The standard says "any category except output iterator."
    // This does not mean that the iterator cannot model the
    // OutputIterator concept; it means that we're requiring an
    // InputIterator.
    ForwardIterator const_iterator = typename X::const_iterator;
    requires SameType<InputIterator<const_iterator>::value_type, value_type>,
            Convertible<iterator, const_iterator>;

    SignedIntegralLike difference_type = typename X::difference_type;
    requires SameType<difference_type, 
                     InputIterator<iterator>::difference_type>,
            SameType<difference_type, 
            InputIterator<const_iterator>::difference_type>;

    // Note: We use sizeof() to try to describe the (partially semantic)
    // requirement that "size_type can represent any non-negative value
    // of difference_type". This may or may not be legitimate.
    UnsignedIntegralLike size_type = typename X::size_type;
    requires UnsignedIntegralLike<size_type>,
            std::True<sizeof(size_type) >= sizeof(difference_type)>;

    iterator       X::begin();
    const_iterator X::begin() const;
    iterator       X::end();
    const_iterator X::end() const;

    void X::swap(X& other);

    size_type X::size() const;
    size_type X::max_size() const;
    bool X::empty() const;
  };

  template<typename X>
  concept_map RandomAccessIterator<const X*>
  {
    typedef X value_type;
    typedef std::ptrdiff_t difference_type;
    typedef const X& reference;
    typedef const X* pointer;
  };

  template<typename X>
  concept_map MutableRandomAccessIterator<X*>
  {
    typedef X value_type;
    typedef std::ptrdiff_t difference_type;
    typedef X& reference;
    typedef X* pointer;
  };

  /**
   * The @c Range concept is used by the compiler to iterate over the
   * values in a container with the C++0x "for" loop. To enable
   * iteration over a new container-like entity, create a new concept
   * map for the Range concept that provides begin and end iterators.
   */
  concept Range<typename X> {
    InputIterator iterator;
    iterator begin(X&);
    iterator end(X&);
  }
  
  /**
   * This concept map enables iteration over all values in an array.
   */
  template<typename T, size_t N>
  concept_map Range<T[N]> {
    typedef T* iterator;
    T* begin(T (&array)[N]) { return array; }
    T* end(T (&array)[N])   { return array + N; }
  }
   
  /**
   * This concept map enables iteration over all values in a container.
   */
  template<Container X>
  concept_map Range<X> {
    typedef X::iterator iterator;
    iterator begin(X& x) { return x.begin(); }
    iterator end(X& x) { return x.end(); }
  }
  
  /**
   * This concept map enables iteration over all values in a constant
   * container.
   */
  template<Container X>
  concept_map Range<const X> {
    typedef X::const_iterator iterator;
    iterator begin(const X& x) { return x.begin(); }
    iterator end(const X& x) { return x.end(); }
  }
}  // namespace std
#endif // ndef _GLIBCXX_NO_CONCEPTS

#endif // _ITERATOR_CONCEPTS_H
